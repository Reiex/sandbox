#include <SandBox/SandBox.hpp>


SandBox::SandBox(const std::string& title, SandBoxInitFunc initFunc, SandBoxUpdateFunc updateFunc, SandBoxEndFunc endFunc) :
	_infos(),
	_title(title),
	_initFunc(initFunc),
	_updateFunc(updateFunc),
	_endFunc(endFunc)
{
}

void SandBox::run()
{
	// Open window and set context

	spl::Window window(_res.x, _res.y, _title, _debug);
	spl::Context* context = window.getContext();
	spl::Context::setCurrentContext(context);
	context->setFaceCulling(spl::FaceCulling::Disabled);

	// Set scene

	spl::Texture2D background("assets/images/cylindric_skybox3.png");

	constexpr scp::f32vec3 lightColor = { 1.f, 0.8f, 0.7f };
	const scp::f32vec3 lightDir = scp::normalize(scp::f32vec3(-2.f, -1.f, -1.f));
	lys::LightSun light(lightDir.x, lightDir.y, lightDir.z, lightColor.x, lightColor.y, lightColor.z, 5.f);
	lys::Scene scene(_res.x, _res.y);
	scene.setBackgroundFlatColor(_clearColor.x, _clearColor.y, _clearColor.z);
	scene.setBackgroundEquirectangular(&background);
	scene.addLight(&light);
	
	lys::CameraPerspective camera(_res.x, _res.y, 1.f, 0.1f, 5.f);
	scene.setCamera(&camera);

	lys::Mesh<> screenMesh(
		{
			{{-1.f, -1.f, 0.f, 1.f}, {0.f}, {0.f}, {0.f, 0.f, 0.f, 0.f}},
			{{ 1.f, -1.f, 0.f, 1.f}, {0.f}, {0.f}, {1.f, 0.f, 0.f, 0.f}},
			{{-1.f,  1.f, 0.f, 1.f}, {0.f}, {0.f}, {0.f, 1.f, 0.f, 0.f}},
			{{ 1.f,  1.f, 0.f, 1.f}, {0.f}, {0.f}, {1.f, 1.f, 0.f, 0.f}}
		},
		spl::BufferStorageFlags::None,
		{ 0, 1, 2, 2, 1, 3 },
		spl::BufferStorageFlags::None
	);

	spl::ShaderProgram screenShader("assets/shaders/screen.vert", "assets/shaders/screen.frag");
	spl::ShaderProgram::bind(&screenShader);

	// Call initialization

	_infos.window = &window;
	_infos.scene = &scene;
	_infos.camera = &camera;
	_infos.dt = 0.0;
	_infos.user = nullptr;

	_initFunc(_infos);

	// Main loop
	
	std::chrono::high_resolution_clock clock;
	std::chrono::time_point start = clock.now();
	while (!window.shouldClose())
	{
		// Handle events

		spl::Event* rawEvent = nullptr;
		while (window.pollEvent(rawEvent))
		{
			switch (rawEvent->type)
			{
				case spl::EventType::ResizeEvent:
				{
					spl::ResizeEvent event = rawEvent->specialize<spl::EventType::ResizeEvent>();
					context->setViewport(0, 0, event.size.x, event.size.y);
					scene.resize(event.size.x, event.size.y);
					camera.setAspect(event.size.x, event.size.y);
					break;
				}
			}
		}

		// Handle debug

		spl::DebugMessage* debugMessage = nullptr;
		while (context->pollDebugMessage(debugMessage))
		{
			std::cout << debugMessage->descr << std::endl;
			std::cout << ">\n";
		}

		// Update
		
		_infos.dt = std::chrono::duration_cast<std::chrono::nanoseconds>((clock.now() - start)).count() * 1e-9;
		start = clock.now();

		_updateFunc(_infos);

		// Update display

		scene.render();

		screenShader.setUniform("u_scene", 0, scene.getRenderTexture());
		screenMesh.draw();

		window.display();
		spl::Framebuffer::clear();
	}

	_endFunc(_infos);
}


float random(float min, float max)
{
	return min + (static_cast<float>(rand()) / RAND_MAX) * (max - min);
}

void createVertexGrid(uint64_t n, std::vector<lys::VertexDefaultMesh>& vertices, std::vector<uint32_t>& indices)
{
	vertices.resize(n * n);
	indices.resize((n - 1) * (n - 1) * 6);

	for (uint64_t i = 0; i < n - 1; ++i)
	{
		for (uint64_t j = 0; j < n - 1; ++j)
		{
			indices[(i * (n - 1) + j) * 6 + 0] = (i + 1) * n + j;
			indices[(i * (n - 1) + j) * 6 + 1] = i * n + j;
			indices[(i * (n - 1) + j) * 6 + 2] = i * n + j + 1;
			indices[(i * (n - 1) + j) * 6 + 3] = (i + 1) * n + j + 1;
			indices[(i * (n - 1) + j) * 6 + 4] = (i + 1) * n + j;
			indices[(i * (n - 1) + j) * 6 + 5] = i * n + j + 1;
		}
	}

	const float normRatio = 2.f / (n - 1);
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j)
		{
			vertices[i * n + j].position = scp::f32vec4(scp::f32vec3(i, 0, j) * normRatio - 1.f, 1.f);
			vertices[i * n + j].normal = { 0.f, 1.f, 0.f, 0.f };
			vertices[i * n + j].tangent = { 1.f, 0.f, 0.f, 0.f };
			vertices[i * n + j].texCoords = scp::f32vec4(scp::f32vec2(i, j) * normRatio - 1.f, 0.f, 0.f);
		}
	}
}

void updateVertexGrid(uint64_t n, std::vector<lys::VertexDefaultMesh>& vertices, const scp::f32vec3* pos)
{
	auto it = vertices.begin();
	const scp::f32vec3* itPos = pos;
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j, ++it, ++itPos)
		{
			it->position.xyz() = *itPos;

			if (i == 0 || j == 0)
			{
				it->normal.xyz() = scp::normalize(scp::cross(*(itPos + 1) - *itPos , *(itPos + n) - *itPos));
			}
			else
			{
				it->normal.xyz() = scp::normalize(scp::cross(*(itPos - 1) - *itPos , *(itPos - n) - *itPos));
			}
		}
	}
}

void marchingCube(const bool* data, const scp::i32vec3& size, std::vector<lys::VertexDefaultMesh>& vertices)
{
	static const scp::f32vec3 confPos[12] = {
		{ 0.5f, 0.0f, 0.0f },
		{ 0.0f, 0.5f, 0.0f },
		{ 1.0f, 0.5f, 0.0f },
		{ 0.5f, 1.0f, 0.0f },
		{ 0.0f, 0.0f, 0.5f },
		{ 1.0f, 0.0f, 0.5f },
		{ 0.0f, 1.0f, 0.5f },
		{ 1.0f, 1.0f, 0.5f },
		{ 0.5f, 0.0f, 1.0f },
		{ 0.0f, 0.5f, 1.0f },
		{ 1.0f, 0.5f, 1.0f },
		{ 0.5f, 1.0f, 1.0f }
	};

	static const std::vector<uint8_t> confTable[256] = {
		{},
		{ 0, 1, 4 },
		{ 0, 5, 2 },
		{ 1, 4, 5, 1, 5, 2 },
		{ 1, 3, 6 },
		{ 0, 3, 6, 0, 6, 4 },
		{ 1, 3, 6, 0, 5, 2 },
		{ 4, 5, 6, 6, 5, 3, 3, 5, 2 },
		{ 3, 2, 7 },
		{ 0, 1, 4, 3, 2, 7 },
		{ 0, 5, 7, 7, 3, 0 },
		{ 5, 7, 4, 4, 7, 1, 1, 7, 3 },
		{ 1, 2, 6, 2, 7, 6 },
		{ 6, 4, 7, 7, 4, 2, 2, 4, 0 },
		{ 7, 6, 5, 5, 6, 0, 0, 6, 1 },
		{ 4, 5, 6, 6, 5, 7 },
		{ 9, 8, 4 },
		{ 0, 1, 9, 0, 9, 8 },
		{ 0, 5, 2, 9, 8, 4 },
		{ 1, 9, 2, 2, 9, 5, 5, 9, 8 },
		{ 1, 3, 6, 9, 8, 4 },
		{ 0, 3, 8, 8, 3, 9, 9, 3, 6 },
		{ 0, 5, 2, 1, 3, 6, 9, 8, 4 },
		{ 5, 2, 8, 8, 2, 9, 9, 2, 3, 3, 6, 9 },
		{ 3, 2, 7, 9, 8, 4 },
		{ 0, 1, 9, 0, 9, 8, 3, 2, 7 },
		{ 0, 5, 7, 7, 3, 0, 9, 8, 4 },
		{ 9, 8, 1, 1, 8, 5, 5, 3, 1, 3, 5, 7 },
		{ 1, 2, 6, 2, 7, 6, 9, 8, 4 },
		{ 8, 0, 9, 9, 0, 6, 6, 0, 2, 2, 7, 6 },
		{ 7, 6, 5, 5, 6, 0, 0, 6, 1, 9, 8, 4 },
		{ 7, 6, 5, 5, 6, 8, 8, 6, 9 },
		{ 5, 8, 10 },
		{ 0, 1, 4, 5, 8, 10 },
		{ 2, 0, 10, 10, 0, 8 },
		{ 2, 1, 10, 10, 1, 8, 8, 1, 4 },
		{ 1, 3, 6, 5, 8, 10 },
		{ 0, 3, 6, 0, 6, 4, 5, 8, 10 },
		{ 1, 3, 6, 2, 0, 10, 10, 0, 8 },
		{ 10, 2, 8, 8, 2, 4, 4, 2, 3, 3, 6, 4 },
		{ 3, 2, 7, 5, 8, 10 },
		{ 0, 1, 4, 3, 2, 7, 5, 8, 10 },
		{ 0, 8, 3, 3, 8, 7, 7, 8, 10 },
		{ 7, 3, 10, 10, 3, 8, 8, 3, 1, 1, 4, 8 },
		{ 1, 2, 6, 2, 7, 6, 5, 8, 10 },
		{ 6, 4, 7, 7, 4, 2, 2, 4, 0, 5, 8, 10 },
		{ 8, 10, 0, 0, 10, 7, 7, 1, 0, 1, 7, 6 },
		{ 6, 4, 7, 7, 4, 10, 4, 8, 10 },
		{ 4, 9, 5, 5, 9, 10 },
		{ 9, 10, 1, 1, 10, 0, 0, 10, 5 },
		{ 10, 2, 9, 9, 2, 4, 4, 2, 0 },
		{ 2, 1, 10, 10, 1, 9 },
		{ 4, 9, 5, 5, 9, 10, 1, 3, 6 },
		{ 10, 5, 9, 9, 5, 0, 0, 6, 5, 6, 0, 3 },
		{ 1, 3, 6, 10, 2, 9, 9, 2, 4, 4, 2, 0 },
		{ 10, 2, 9, 9, 2, 6, 6, 2, 3 },
		{ 3, 2, 7, 4, 9, 5, 5, 9, 10 },
		{ 3, 2, 7, 9, 10, 1, 1, 10, 0, 0, 10, 5 },
		{ 3, 0, 7, 7, 0, 10, 10, 0, 4, 4, 9, 10 },
		{ 9, 10, 1, 1, 10, 3, 3, 10, 7 },
		{ 1, 2, 6, 2, 7, 6, 4, 9, 5, 5, 9, 10 },
		{ 5, 0, 10, 10, 0, 9, 9, 0, 6, 6, 0, 7, 7, 0, 2 },
		{ 6, 3, 9, 9, 3, 10, 10, 3, 5, 5, 3, 4, 4, 3, 1 },
		{ 7, 6, 10, 10, 6, 9 },
		{ 9, 6, 11 },
		{ 0, 1, 4, 9, 6, 11 },
		{ 0, 5, 2, 9, 6, 11 },
		{ 1, 4, 5, 1, 5, 2, 9, 6, 11 },
		{ 1, 3, 9, 9, 3, 11 },
		{ 3, 11, 0, 0, 11, 4, 4, 11, 9 },
		{ 0, 5, 2, 1, 3, 9, 9, 3, 11 },
		{ 11, 9, 3, 3, 9, 4, 4, 2, 3, 2, 4, 5 },
		{ 3, 2, 7, 9, 6, 11 },
		{ 0, 1, 4, 3, 2, 7, 9, 6, 11 },
		{ 0, 5, 7, 7, 3, 0, 9, 6, 11 },
		{ 5, 7, 4, 4, 7, 1, 1, 7, 3, 9, 6, 11 },
		{ 1, 2, 9, 9, 2, 11, 11, 2, 7 },
		{ 4, 0, 9, 9, 0, 11, 11, 0, 2, 2, 7, 11 },
		{ 9, 1, 11, 11, 1, 7, 7, 1, 0, 0, 5, 7 },
		{ 5, 7, 4, 4, 7, 9, 9, 7, 11 },
		{ 4, 6, 8, 8, 6, 11 },
		{ 8, 0, 11, 11, 0, 6, 6, 0, 1 },
		{ 0, 5, 2, 4, 6, 8, 8, 6, 11 },
		{ 11, 8, 6, 6, 8, 1, 1, 8, 5, 5, 2, 1 },
		{ 11, 8, 3, 3, 8, 1, 1, 8, 4 },
		{ 11, 8, 3, 3, 8, 0 },
		{ 0, 5, 2, 11, 8, 3, 3, 8, 1, 1, 8, 4 },
		{ 11, 8, 3, 3, 8, 2, 2, 8, 5 },
		{ 3, 2, 7, 4, 6, 8, 8, 6, 11 },
		{ 3, 2, 7, 8, 0, 11, 11, 0, 6, 6, 0, 1 },
		{ 0, 5, 7, 7, 3, 0, 4, 6, 8, 8, 6, 11 },
		{ 3, 1, 7, 7, 1, 5, 5, 1, 8, 8, 1, 11, 11, 1, 6 },
		{ 2, 7, 1, 1, 7, 11, 11, 4, 1, 4, 11, 8 },
		{ 8, 0, 11, 11, 0, 7, 7, 0, 2 },
		{ 4, 1, 8, 8, 1, 11, 11, 1, 7, 7, 1, 5, 5, 1, 0 },
		{ 7, 11, 5, 5, 11, 8 },
		{ 5, 8, 10, 9, 6, 11 },
		{ 0, 1, 4, 5, 8, 10, 9, 6, 11 },
		{ 2, 0, 10, 10, 0, 8, 9, 6, 11 },
		{ 2, 1, 10, 10, 1, 8, 8, 1, 4, 9, 6, 11 },
		{ 5, 8, 10, 1, 3, 9, 9, 3, 11 },
		{ 5, 8, 10, 3, 11, 0, 0, 11, 4, 4, 11, 9 },
		{ 2, 0, 10, 10, 0, 8, 1, 3, 9, 9, 3, 11 },
		{ 9, 4, 11, 11, 4, 3, 3, 4, 2, 2, 4, 10, 10, 4, 8 },
		{ 3, 2, 7, 5, 8, 10, 9, 6, 11 },
		{ 0, 1, 4, 3, 2, 7, 5, 8, 10, 9, 6, 11 },
		{ 0, 8, 3, 3, 8, 7, 7, 8, 10, 9, 6, 11 },
		{ 7, 3, 10, 10, 3, 8, 8, 3, 1, 1, 4, 8, 9, 6, 11 },
		{ 5, 8, 10, 1, 2, 9, 9, 2, 11, 11, 2, 7 },
		{ 5, 8, 10, 4, 0, 9, 9, 0, 11, 11, 0, 2, 2, 7, 11 },
		{ 10, 7, 8, 8, 7, 0, 0, 7, 1, 1, 7, 9, 9, 7, 11 },
		{ 11, 9, 7, 7, 9, 4, 10, 7, 8, 8, 7, 4 },
		{ 4, 6, 5, 5, 6, 10, 10, 6, 11 },
		{ 6, 11, 1, 1, 11, 0, 0, 11, 10, 10, 5, 0 },
		{ 2, 0, 10, 10, 0, 4, 4, 11, 10, 11, 4, 6 },
		{ 2, 1, 10, 10, 1, 11, 11, 1, 6 },
		{ 3, 11, 1, 1, 11, 4, 4, 11, 10, 10, 5, 4 },
		{ 3, 11, 0, 0, 11, 5, 5, 11, 10 },
		{ 0, 4, 2, 2, 4, 10, 10, 4, 11, 11, 4, 3, 3, 4, 1 },
		{ 2, 3, 10, 10, 3, 11 },
		{ 3, 2, 7, 4, 6, 5, 5, 6, 10, 10, 6, 11 },
		{ 3, 2, 7, 6, 11, 1, 1, 11, 0, 0, 11, 10, 10, 5, 0 },
		{ 11, 10, 6, 6, 10, 4, 4, 10, 0, 0, 10, 3, 3, 10, 7 },
		{ 6, 11, 1, 1, 11, 10, 3, 1, 7, 7, 1, 10 },
		{ 7, 11, 2, 2, 11, 1, 1, 11, 4, 4, 11, 5, 5, 11, 10 },
		{ 10, 5, 11, 11, 5, 0, 7, 11, 2, 2, 11, 0 },
		{ 11, 10, 7, 1, 0, 4 },
		{ 11, 10, 7 },
		{ 7, 10, 11 },
		{ 0, 1, 4, 7, 10, 11 },
		{ 0, 5, 2, 7, 10, 11 },
		{ 1, 4, 5, 1, 5, 2, 7, 10, 11 },
		{ 1, 3, 6, 7, 10, 11 },
		{ 0, 3, 6, 0, 6, 4, 7, 10, 11 },
		{ 1, 3, 6, 0, 5, 2, 7, 10, 11 },
		{ 4, 5, 6, 6, 5, 3, 3, 5, 2, 7, 10, 11 },
		{ 2, 10, 3, 3, 10, 11 },
		{ 0, 1, 4, 2, 10, 3, 3, 10, 11 },
		{ 3, 0, 11, 11, 0, 10, 10, 0, 5 },
		{ 11, 3, 10, 10, 3, 5, 5, 3, 1, 1, 4, 5 },
		{ 2, 10, 1, 1, 10, 6, 6, 10, 11 },
		{ 10, 11, 2, 2, 11, 6, 6, 0, 2, 0, 6, 4 },
		{ 6, 1, 11, 11, 1, 10, 10, 1, 0, 0, 5, 10 },
		{ 4, 5, 6, 6, 5, 11, 11, 5, 10 },
		{ 9, 8, 4, 7, 10, 11 },
		{ 0, 1, 9, 0, 9, 8, 7, 10, 11 },
		{ 0, 5, 2, 9, 8, 4, 7, 10, 11 },
		{ 1, 9, 2, 2, 9, 5, 5, 9, 8, 7, 10, 11 },
		{ 1, 3, 6, 9, 8, 4, 7, 10, 11 },
		{ 0, 3, 8, 8, 3, 9, 9, 3, 6, 7, 10, 11 },
		{ 0, 5, 2, 1, 3, 6, 9, 8, 4, 7, 10, 11 },
		{ 5, 2, 8, 8, 2, 9, 9, 2, 3, 3, 6, 9, 7, 10, 11 },
		{ 9, 8, 4, 2, 10, 3, 3, 10, 11 },
		{ 0, 1, 9, 0, 9, 8, 2, 10, 3, 3, 10, 11 },
		{ 9, 8, 4, 3, 0, 11, 11, 0, 10, 10, 0, 5 },
		{ 8, 5, 9, 9, 5, 1, 1, 5, 3, 3, 5, 11, 11, 5, 10 },
		{ 9, 8, 4, 2, 10, 1, 1, 10, 6, 6, 10, 11 },
		{ 11, 6, 10, 10, 6, 2, 2, 6, 0, 0, 6, 8, 8, 6, 9 },
		{ 9, 8, 4, 6, 1, 11, 11, 1, 10, 10, 1, 0, 0, 5, 10 },
		{ 9, 8, 6, 6, 8, 5, 11, 6, 10, 10, 6, 5 },
		{ 8, 11, 5, 5, 11, 7 },
		{ 0, 1, 4, 8, 11, 5, 5, 11, 7 },
		{ 8, 11, 0, 0, 11, 2, 2, 11, 7 },
		{ 1, 4, 2, 2, 4, 8, 8, 7, 2, 7, 8, 11 },
		{ 1, 3, 6, 8, 11, 5, 5, 11, 7 },
		{ 0, 3, 6, 0, 6, 4, 8, 11, 5, 5, 11, 7 },
		{ 1, 3, 6, 8, 11, 0, 0, 11, 2, 2, 11, 7 },
		{ 7, 2, 11, 11, 2, 8, 8, 2, 4, 4, 2, 6, 6, 2, 3 },
		{ 11, 3, 8, 3, 8, 2, 2, 8, 0 },
		{ 0, 1, 4, 11, 3, 8, 3, 8, 2, 2, 8, 0 },
		{ 11, 3, 8, 8, 3, 0 },
		{ 11, 3, 8, 8, 3, 4, 4, 3, 1 },
		{ 8, 11, 5, 5, 11, 2, 2, 11, 6, 6, 1, 2 },
		{ 0, 2, 4, 4, 2, 6, 6, 2, 11, 11, 2, 8, 8, 2, 5 },
		{ 8, 11, 0, 0, 11, 1, 1, 11, 6 },
		{ 8, 11, 4, 4, 11, 6 },
		{ 5, 4, 7, 7, 4, 11, 11, 4, 9 },
		{ 7, 5, 11, 11, 5, 9, 9, 5, 0, 0, 1, 9 },
		{ 2, 0, 7, 7, 0, 11, 11, 0, 4, 4, 9, 11 },
		{ 1, 9, 2, 2, 9, 7, 7, 9, 11 },
		{ 1, 3, 6, 5, 4, 7, 7, 4, 11, 11, 4, 9 },
		{ 6, 9, 3, 3, 9, 0, 0, 9, 5, 5, 9, 7, 7, 9, 11 },
		{ 1, 3, 6, 2, 0, 7, 7, 0, 11, 11, 0, 4, 4, 9, 11 },
		{ 11, 7, 9, 9, 7, 2, 6, 9, 11, 11, 9, 2 },
		{ 3, 2, 11, 11, 2, 5, 5, 9, 11, 9, 5, 4 },
		{ 2, 5, 3, 3, 5, 11, 11, 5, 9, 9, 5, 1, 1, 5, 0 },
		{ 3, 0, 11, 11, 0, 9, 9, 0, 4 },
		{ 11, 3, 9, 9, 3, 1 },
		{ 9, 11, 4, 4, 11, 5, 5, 11, 2, 2, 11, 1, 1, 11, 6 },
		{ 0, 2, 5, 6, 9, 11 },
		{ 6, 1, 11, 11, 1, 0, 9, 11, 4, 4, 11, 0 },
		{ 6, 9, 11 },
		{ 9, 6, 10, 10, 6, 7 },
		{ 0, 1, 4, 9, 6, 10, 10, 6, 7 },
		{ 0, 5, 2, 9, 6, 10, 10, 6, 7 },
		{ 1, 4, 5, 1, 5, 2, 9, 6, 10, 10, 6, 7 },
		{ 9, 1, 10, 10, 1, 7, 7, 1, 3 },
		{ 0, 3, 4, 4, 3, 9, 9, 3, 7, 7, 10, 9 },
		{ 0, 5, 2, 9, 1, 10, 10, 1, 7, 7, 1, 3 },
		{ 2, 3, 5, 5, 3, 4, 4, 3, 9, 9, 3, 10, 10, 3, 7 },
		{ 10, 9, 2, 2, 9, 3, 3, 9, 6 },
		{ 0, 1, 4, 10, 9, 2, 2, 9, 3, 3, 9, 6 },
		{ 9, 6, 10, 10, 6, 3, 3, 5, 10, 5, 3, 0 },
		{ 6, 3, 9, 9, 3, 10, 10, 3, 5, 5, 3, 4, 4, 3, 1 },
		{ 9, 1, 10, 10, 1, 2 },
		{ 10, 9, 2, 2, 9, 0, 0, 9, 4 },
		{ 9, 1, 10, 10, 1, 5, 5, 1, 0 },
		{ 10, 9, 5, 5, 9, 4 },
		{ 6, 7, 4, 4, 7, 8, 8, 7, 10 },
		{ 10, 5, 9, 9, 5, 0, 0, 6, 9, 6, 0, 3 },
		{ 0, 5, 2, 6, 7, 4, 4, 7, 8, 8, 7, 10 },
		{ 10, 8, 7, 7, 8, 6, 6, 8, 1, 1, 8, 2, 2, 8, 5 },
		{ 7, 10, 3, 3, 10, 1, 1, 10, 8, 8, 4, 1 },
		{ 0, 3, 8, 8, 3, 10, 10, 3, 7 },
		{ 0, 5, 2, 7, 10, 3, 3, 10, 1, 1, 10, 8, 8, 4, 1 },
		{ 7, 10, 3, 3, 10, 8, 2, 3, 5, 5, 3, 8 },
		{ 2, 10, 3, 3, 10, 6, 6, 10, 8, 8, 4, 6 },
		{ 1, 6, 0, 0, 6, 8, 8, 6, 10, 10, 6, 2, 2, 6, 3 },
		{ 5, 10, 0, 0, 10, 3, 3, 10, 6, 6, 10, 4, 4, 10, 8 },
		{ 8, 5, 10, 1, 6, 3 },
		{ 2, 10, 1, 1, 10, 4, 4, 10, 8 },
		{ 8, 10, 0, 0, 10, 2 },
		{ 8, 4, 10, 10, 4, 1, 5, 10, 0, 0, 10, 1 },
		{ 10, 8, 5 },
		{ 7, 5, 6, 6, 5, 9, 9, 5, 8 },
		{ 0, 1, 4, 7, 5, 6, 6, 5, 9, 9, 5, 8 },
		{ 2, 0, 10, 10, 0, 4, 4, 11, 10, 11, 4, 6 },
		{ 4, 8, 1, 1, 8, 2, 2, 8, 7, 7, 8, 6, 6, 8, 9 },
		{ 1, 3, 9, 9, 3, 7, 7, 8, 9, 8, 7, 5 },
		{ 8, 9, 5, 5, 9, 7, 7, 9, 3, 3, 9, 0, 0, 9, 4 },
		{ 3, 7, 1, 1, 7, 9, 9, 7, 8, 8, 7, 0, 0, 7, 2 },
		{ 4, 8, 9, 2, 3, 7 },
		{ 5, 8, 2, 2, 8, 3, 3, 8, 9, 9, 6, 3 },
		{ 0, 1, 4, 5, 8, 2, 2, 8, 3, 3, 8, 9, 9, 6, 3 },
		{ 0, 8, 3, 3, 8, 6, 6, 8, 9 },
		{ 9, 11, 4, 4, 11, 5, 5, 11, 2, 2, 11, 1, 1, 11, 6 },
		{ 1, 2, 9, 9, 2, 8, 8, 2, 5 },
		{ 5, 8, 2, 2, 8, 9, 0, 2, 4, 4, 2, 9 },
		{ 8, 9, 0, 0, 9, 1 },
		{ 4, 8, 9 },
		{ 6, 7, 4, 4, 7, 5 },
		{ 7, 5, 6, 6, 5, 1, 1, 5, 0 },
		{ 6, 7, 4, 4, 7, 0, 0, 7, 2 },
		{ 2, 1, 7, 7, 1, 6 },
		{ 5, 4, 7, 7, 4, 3, 3, 4, 1 },
		{ 0, 3, 5, 5, 3, 7 },
		{ 1, 3, 4, 4, 3, 7, 0, 4, 2, 2, 4, 7 },
		{ 2, 3, 7 },
		{ 4, 6, 5, 5, 6, 2, 2, 6, 3 },
		{ 3, 2, 6, 6, 2, 5, 1, 6, 0, 0, 6, 5 },
		{ 3, 0, 6, 6, 0, 4 },
		{ 1, 6, 3 },
		{ 2, 5, 1, 1, 5, 4 },
		{ 0, 2, 5 },
		{ 0, 4, 1 },
		{},
	};

	const uint64_t sizeXY = size.x * size.y;

	vertices.clear();
	for (int32_t i = -1; i < size.x; ++i)
	{
		for (int32_t j = -1; j < size.y; ++j)
		{
			for (int32_t k = -1; k < size.z; ++k)
			{
				scp::i32vec3 cubeIndices[8] = {
					{i, j, k},
					{i+1, j, k},
					{i, j+1, k},
					{i+1, j+1, k},
					{i, j, k+1},
					{i+1, j, k+1},
					{i, j+1, k+1},
					{i+1, j+1, k+1}
				};

				uint8_t confIndex = 0;
				for (uint8_t l = 0; l < 8; ++l)
				{
					const scp::i32vec3& indices = cubeIndices[l];

					confIndex >>= 1;
					if (indices.x >= 0 && indices.x < size.x && indices.y >= 0 && indices.y < size.y && indices.z >= 0 && indices.z < size.z && data[indices.x + indices.y * size.x + indices.z * sizeXY])
					{
						confIndex |= 128;
					}
				}

				const std::vector<uint8_t>& conf = confTable[confIndex];

				for (uint8_t l = 0; l < conf.size(); l += 3)
				{
					lys::VertexDefaultMesh a, b, c;
					float ratio = 2.f / std::max({ size.x, size.y, size.z });
					
					a.position = scp::f32vec4((confPos[conf[l]] + scp::f32vec3(i, j, k)) * ratio - 1.f, 1.f);
					b.position = scp::f32vec4((confPos[conf[l + 1]] + scp::f32vec3(i, j, k)) * ratio - 1.f, 1.f);
					c.position = scp::f32vec4((confPos[conf[l + 2]] + scp::f32vec3(i, j, k)) * ratio - 1.f, 1.f);

					a.normal = scp::f32vec4(scp::cross(c.position.xyz() - b.position.xyz(), a.position.xyz() - b.position.xyz()), 0.f);
					b.normal = a.normal;
					c.normal = a.normal;

					vertices.push_back(a);
					vertices.push_back(b);
					vertices.push_back(c);
				}
			}
		}
	}
}
