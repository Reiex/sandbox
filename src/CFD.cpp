#include <common.hpp>

enum class CellType
{
	Inflow,
	Outflow,
	Solid,
	Free
};

struct FoMeCell
{
	spl::vec3 v;
	float p;
	CellType type;
	bool hasLiquid;
};

struct FoMeParticle
{
	spl::vec3 pos;
};

void initFoMe(uint64_t n, scp::Tensor<FoMeCell>& cells, std::vector<FoMeParticle>& particles)
{
	auto itCell = cells.begin();
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j)
		{
			for (uint64_t k = 0; k < n; ++k, ++itCell)
			{
				if (i == 0 || j == 0 || k == 0 || i == n - 1 || j == n - 1 || k == n - 1)
				{
					itCell->v = { 0.f, 0.f, 0.f };
					itCell->p = 0.f;
					itCell->type = CellType::Solid;
					itCell->hasLiquid = false;
				}
				else if (k < n / 2 && j > 2 * n / 3)
				{
					itCell->v = { 0.f, 0.f, 0.f };
					itCell->p = 1.f;
					itCell->type = CellType::Free;
					itCell->hasLiquid = true;
					for (uint64_t l = 0; l < 8; ++l)
					{
						FoMeParticle particle;
						particle.pos.x = i + float(rand()) / RAND_MAX;
						particle.pos.y = j + float(rand()) / RAND_MAX;
						particle.pos.z = k + float(rand()) / RAND_MAX;
						particles.push_back(particle);
					}
				}
				else
				{
					itCell->v = { 0.f, 0.f, 0.f };
					itCell->p = 1.f;
					itCell->type = CellType::Free;
					itCell->hasLiquid = false;
				}
			}
		}
	}
}

void updateFoMe(uint64_t n, scp::Tensor<FoMeCell>& cells, std::vector<FoMeParticle>& particles, std::vector<spl::DefaultVertex>& vertices)
{
	constexpr float nu = 0.f;

	// Compute dt

	float maxV = 1.f;
	for (const FoMeCell& cell : cells)
	{
		maxV = std::max({ maxV, std::abs(cell.v.x), std::abs(cell.v.y), std::abs(cell.v.z) });
	}

	const float dl = 1.f / n;
	const float dt = 0.5f * dl / maxV;

	static float t = 0.f;
	t += dt;
	// std::cout << t << std::endl;
	std::cout << particles[0].pos.x << ", " << particles[0].pos.y << ", " << particles[0].pos.z << std::endl;

	// Prepare cells for computation

	FoMeCell* itCell = cells.getData();
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j)
		{
			for (uint64_t k = 0; k < n; ++k, ++itCell)
			{
				FoMeCell* itLeft = itCell - n * n;	// u-
				FoMeCell* itRight = itCell + n * n;	// u+
				FoMeCell* itDown = itCell - n;		// v-
				FoMeCell* itUp = itCell + n;		// v+
				FoMeCell* itBack = itCell - 1;		// w-
				FoMeCell* itFront = itCell + 1;		// w+

				switch (itCell->type)
				{
					case CellType::Solid:
					{
						itCell->v = { 0.f, 0.f, 0.f };
						if (i != n - 1) itRight->v.x = 0.f;
						if (j != n - 1) itUp->v.y = 0.f;
						if (k != n - 1) itFront->v.z = 0.f;

						uint8_t count = 0;
						float pressure = 0.f;

						if (i != 0 && itLeft->hasLiquid) { ++count; pressure += itLeft->p; }
						if (i != n - 1 && itRight->hasLiquid) { ++count; pressure += itRight->p; }
						if (j != 0 && itDown->hasLiquid) { ++count; pressure += itDown->p; }
						if (j != n - 1 && itUp->hasLiquid) { ++count; pressure += itUp->p; }
						if (k != 0 && itBack->hasLiquid) { ++count; pressure += itBack->p; }
						if (k != n - 1 && itFront->hasLiquid) { ++count; pressure += itFront->p; }

						itCell->p = count ? pressure / count : 1.f;

						break;
					}
					case CellType::Free:
					{
						// uint8_t count = itLeft->hasLiquid + itRight->hasLiquid + itDown->hasLiquid + itUp->hasLiquid + itBack->hasLiquid + itFront->hasLiquid;
						// 
						// if (itCell->hasLiquid && count != 6)
						// {
						// 	float shared = 0.f;
						// 
						// 	if (itLeft->hasLiquid && !itRight->hasLiquid) { itRight->v.x = itCell->v.x; }
						// 	else if (!itLeft->hasLiquid && itRight->hasLiquid) { itCell->v.x = itRight->v.x; }
						// 	else if (itLeft->hasLiquid && itRight->hasLiquid) { shared += itCell->v.x - itRight->v.x; }
						// 
						// 	if (itDown->hasLiquid && !itUp->hasLiquid) { itUp->v.y = itCell->v.y; }
						// 	else if (!itDown->hasLiquid && itUp->hasLiquid) { itCell->v.y = itUp->v.y; }
						// 	else if (itDown->hasLiquid && itUp->hasLiquid) { shared += itCell->v.y - itUp->v.y; }
						// 
						// 	if (itBack->hasLiquid && !itFront->hasLiquid) { itFront->v.z = itCell->v.z; }
						// 	else if (!itBack->hasLiquid && itFront->hasLiquid) { itCell->v.z = itFront->v.z; }
						// 	else if (itBack->hasLiquid && itFront->hasLiquid) { shared += itCell->v.z - itFront->v.z; }
						// 
						// 	count = 6 - count;
						// 	
						// 	if (!itLeft->hasLiquid) { itCell->v.x -= shared / count; }
						// 	if (!itRight->hasLiquid) { itRight->v.x += shared / count; }
						// 	if (!itDown->hasLiquid) { itCell->v.y -= shared / count; }
						// 	if (!itUp->hasLiquid) { itUp->v.y += shared / count; }
						// 	if (!itBack->hasLiquid) { itCell->v.z -= shared / count; }
						// 	if (!itFront->hasLiquid) { itFront->v.z += shared / count; }
						// }
					}
				}
			}
		}
	}

	// Compute velocity

	scp::Tensor<spl::vec3> velocities({ n, n, n });

	itCell = cells.getData();
	spl::vec3* itVel = velocities.getData();
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j)
		{
			for (uint64_t k = 0; k < n; ++k, ++itCell, ++itVel)
			{
				*itVel = itCell->v;
				if (itCell->hasLiquid)
				{
					FoMeCell* itLeft = itCell - n * n;	// u-
					FoMeCell* itRight = itCell + n * n;	// u+
					FoMeCell* itDown = itCell - n;		// v-
					FoMeCell* itUp = itCell + n;		// v+
					FoMeCell* itBack = itCell - 1;		// w-
					FoMeCell* itFront = itCell + 1;		// w+

					itVel->x += dt * (
						(
							-(itRight->v.x * itRight->v.x - itCell->v.x * itCell->v.x
								+ itUp->v.x * itUp->v.y - itCell->v.x * itCell->v.y
								+ itFront->v.x * itFront->v.z - itCell->v.x * itCell->v.z
								+ itRight->p - itCell->p)
							+ nu * (
								itRight->v.x - 2.f * itCell->v.x + itLeft->v.x
								+ itUp->v.x - 2.f * itCell->v.x + itDown->v.x
								+ itFront->v.x - 2.f * itCell->v.x + itBack->v.x
								) / dl
							) / dl
						);

					itVel->y += + dt * (
						(
							-(itRight->v.y * itRight->v.x - itCell->v.y * itCell->v.x
								+ itUp->v.y * itUp->v.y - itCell->v.y * itCell->v.y
								+ itFront->v.y * itFront->v.z - itCell->v.y * itCell->v.z
								+ itUp->p - itCell->p)
							+ nu * (
								itRight->v.y - 2.f * itCell->v.y + itLeft->v.y
								+ itUp->v.y - 2.f * itCell->v.y + itDown->v.y
								+ itFront->v.y - 2.f * itCell->v.y + itBack->v.y
								) / dl
							) / dl
							- 9.81f
						);

					itVel->z += + dt * (
						(
							-(itRight->v.z * itRight->v.x - itCell->v.z * itCell->v.x
								+ itUp->v.z * itUp->v.y - itCell->v.z * itCell->v.y
								+ itFront->v.z * itFront->v.z - itCell->v.z * itCell->v.z
								+ itFront->p - itCell->p)
							+ nu * (
								itRight->v.z - 2.f * itCell->v.z + itLeft->v.z
								+ itUp->v.z - 2.f * itCell->v.z + itDown->v.z
								+ itFront->v.z - 2.f * itCell->v.z + itBack->v.z
								) / dl
							) / dl
						);
				}
			}
		}
	}

	// Adapt pressure

	for (uint8_t i = 0; i < 10; ++i)
	{
		itVel = velocities.getData();
		itCell = cells.getData();
		for (uint64_t i = 0; i < n; ++i)
		{
			for (uint64_t j = 0; j < n; ++j)
			{
				for (uint64_t k = 0; k < n; ++k, ++itCell, ++itVel)
				{
					if (itCell->hasLiquid)
					{
						spl::vec3* itRight = itVel + n * n;	// u+
						spl::vec3* itUp = itVel + n;		// v+
						spl::vec3* itFront = itVel + 1;		// w+

						float div = (itRight->x - itVel->x + itUp->y - itVel->y + itFront->z - itVel->z) / dl;
						float dp = 1.7f * div * dl * dl / (6.f * dt);

						itRight->x += dt * dp / dl;
						itUp->y += dt * dp / dl;
						itFront->z += dt * dp / dl;
						itVel->x -= dt * dp / dl;
						itVel->y -= dt * dp / dl;
						itVel->z -= dt * dp / dl;
						itCell->p += dp;
					}
				}
			}
		}
	}

	itCell = cells.getData();
	itVel = velocities.getData();
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j)
		{
			for (uint64_t k = 0; k < n; ++k, ++itCell, ++itVel)
			{
				itCell->v = *itVel;
			}
		}
	}

	// Move particles

	for (FoMeParticle& particle : particles)
	{
		particle.pos += dt * velocities.getInterpolated<float, scp::InterpolationMethod::Linear>({ particle.pos.x, particle.pos.y, particle.pos.z });
	}

	// Create/destroy particles

	// Compute full cells

	itCell = cells.getData();
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j)
		{
			for (uint64_t k = 0; k < n; ++k, ++itCell)
			{
				itCell->hasLiquid = false;
			}
		}
	}

	for (FoMeParticle& particle : particles)
	{
		const uint64_t i = particle.pos.x;
		const uint64_t j = particle.pos.y;
		const uint64_t k = particle.pos.z;

		cells[{i, j, k}].hasLiquid = true;
	}

	// Compute new vertices

	bool* mcData = new bool[n * n * n];
	bool* itMcData = mcData;
	itCell = cells.getData();
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j)
		{
			for (uint64_t k = 0; k < n; ++k, ++itMcData, ++itCell)
			{
				*itMcData = itCell->hasLiquid;
			}
		}
	}

	marchingCube(mcData, spl::ivec3{ int32_t(n), int32_t(n), int32_t(n) }, vertices);
	delete[] mcData;
}

void mainFosterMetaxas()
{
	// Open window and set context

	spl::Window window({ 1000, 600 }, "FosterMetaxas");
	spl::Context* context = window.getContext();
	spl::ContextManager::setCurrentContext(context);
	context->setIsDepthTestEnabled(true);
	context->setClearColor({ 0.2f, 0.3f, 0.3f, 1.f });

	// Set scene (shader, camera, ...)

	spl::Shader shader("assets/shaders/main.vert", "assets/shaders/main.frag");
	spl::Shader::bind(shader);

	spl::PerspectiveCamera camera({ 1000, 600 }, 0.1f, 100.f, 1.f);
	camera.setTranslation({ 0.f, 0.f, 3.f });

	spl::vec3 lightDir = -spl::normalize(spl::vec3{ 1.0, 1.0, 1.0 });

	// Initialization

	constexpr uint64_t n = 32;
	scp::Tensor<FoMeCell> cells({ n, n, n });
	std::vector<FoMeParticle> particles;
	initFoMe(n, cells, particles);

	std::vector<spl::DefaultVertex> vertices(n * n * n);
	spl::Mesh<> mesh(vertices);
	spl::Transformable3D meshTransform;

	// Main loop

	while (!window.shouldClose())
	{
		// Handle events

		spl::Event* rawEvent = nullptr;
		while (window.pollEvent(rawEvent))
		{
			switch (rawEvent->type)
			{
				case spl::EventType::ResizeEvent:
				{
					spl::ResizeEvent event = rawEvent->specialize<spl::EventType::ResizeEvent>();
					context->setViewport({ 0, 0 }, event.size);
					camera.setAspect(event.size);
					break;
				}
			}
		}

		// Update

		updateFoMe(n, cells, particles, vertices);
		mesh.updateVertices(vertices);

		// Handle camera

		constexpr float speed = 0.02f;
		if (window.isKeyPressed(spl::KeyboardKey::W)) camera.move(camera.getUpVector() * speed);
		if (window.isKeyPressed(spl::KeyboardKey::S)) camera.move(camera.getUpVector() * -speed);
		if (window.isKeyPressed(spl::KeyboardKey::A)) camera.move(camera.getLeftVector() * speed);
		if (window.isKeyPressed(spl::KeyboardKey::D)) camera.move(camera.getLeftVector() * -speed);
		if (window.isKeyPressed(spl::KeyboardKey::Space)) camera.move(camera.getFrontVector() * speed);
		if (window.isKeyPressed(spl::KeyboardKey::LeftShift)) camera.move(camera.getFrontVector() * -speed);

		camera.lookAt({ 0.f, 0.f, 0.f });

		// Draw

		shader.setUniform("cameraPos", camera.getTranslation());
		shader.setUniform("lightDir", lightDir);

		shader.setUniform("projection", camera.getProjectionMatrix());
		shader.setUniform("view", camera.getViewMatrix());

		shader.setUniform("model", meshTransform.getTransformMatrix());
		mesh.draw(spl::PrimitiveType::Triangles, vertices.size());

		// Update display

		window.display();
		spl::Framebuffer::clear();
	}
}
