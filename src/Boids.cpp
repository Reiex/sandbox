#include <SandBox/SandBox.hpp>

struct Boid
{
	scp::f32vec3 p;
	scp::f32vec3 v;
};

void initBoids(std::vector<Boid>& boids, std::vector<lys::VertexDefaultMesh>& vertices)
{
	for (Boid& boid : boids)
	{
		boid.p.x = random(-1.f, 1.f);
		boid.p.y = random(-1.f, 1.f);
		boid.p.z = random(-1.f, 1.f);
		boid.v.x = random(-1.f, 1.f);
		boid.v.y = random(-1.f, 1.f);
		boid.v.z = random(-1.f, 1.f);
	}

	for (uint64_t i = 0; i < boids.size(); ++i)
	{
		vertices[2 * i].position = scp::f32vec4(boids[i].p, 1.f);
		vertices[2 * i].normal = { 1.f, 1.f, 1.f, 0.f };

		vertices[2 * i + 1].position = scp::f32vec4(boids[i].p + 0.05 * boids[i].v / scp::length(boids[i].v), 1.f);
		vertices[2 * i + 1].normal = { 0.f, 0.f, 0.f, 0.f };
	}
}

void updateBoids(std::vector<Boid>& boids, std::vector<lys::VertexDefaultMesh>& vertices, float speed)
{
	const float dt = speed;
	constexpr float borderHardness = 10.f;

	constexpr float cohesionRadius = 0.6f;
	constexpr float alignmentRadius = 0.15f * cohesionRadius;
	constexpr float separationRadius = 0.05f * cohesionRadius;

	constexpr float cohesionIntensity = 0.3f;
	constexpr float alignmentIntensity = 0.3f;
	constexpr float separationIntensity = 1.f;

	for (Boid& boid : boids)
	{
		for (Boid& other : boids)
		{
			if (&other != &boid)
			{
				scp::f32vec3 r = other.p - boid.p;
				const float d = scp::length(r);
				r /= d;

				if (scp::dot(r, boid.v / scp::length(boid.v)) < -0.8f)
				{
					continue;
				}

				if (d < separationRadius)
				{
					boid.v -= separationIntensity * dt * r;
				}
				else if (d < alignmentRadius)
				{
					boid.v = (1.f - alignmentIntensity * dt) * boid.v + alignmentIntensity * dt * other.v;
				}
				else if (d < cohesionRadius)
				{
					boid.v += cohesionIntensity * dt * r / d;
				}
			}
		}

		boid.p += dt * boid.v;

		if (boid.p.x >= 1.f && boid.v.x > -0.1f) { boid.v.x -= borderHardness * dt; }
		if (boid.p.x <= -1.f && boid.v.x < 0.1f) { boid.v.x += borderHardness * dt; }
		if (boid.p.y >= 1.f && boid.v.y > -0.1f) { boid.v.y -= borderHardness * dt; }
		if (boid.p.y <= -1.f && boid.v.y < 0.1f) { boid.v.y += borderHardness * dt; }
		if (boid.p.z >= 1.f && boid.v.z > -0.1f) { boid.v.z -= borderHardness * dt; }
		if (boid.p.z <= -1.f && boid.v.z < 0.1f) { boid.v.z += borderHardness * dt; }
	}

	for (uint64_t i = 0; i < boids.size(); ++i)
	{
		vertices[2 * i].position.xyz() = boids[i].p;
		vertices[2 * i + 1].position.xyz() = boids[i].p + 0.05 * boids[i].v / scp::length(boids[i].v);
	}
}

struct BoidsInfos
{
	static constexpr uint64_t n = 300;
	std::vector<Boid> boids = std::vector<Boid>(n);
	std::vector<lys::VertexDefaultMesh> vertices = std::vector<lys::VertexDefaultMesh>(n * 2);

	lys::Mesh<>* mesh;

	double speed = 1e-3;
};

void boidsInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new BoidsInfos();

	BoidsInfos& infos = *reinterpret_cast<BoidsInfos*>(sandBoxInfos.user);

	initBoids(infos.boids, infos.vertices);

	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferUsage::StreamDraw, spl::BufferStorageFlags::None);
	// infos.mesh->setPrimitiveType(spl::PrimitiveType::Lines);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void boidsUpdate(SandBoxInfos& sandBoxInfos)
{
	BoidsInfos& infos = *reinterpret_cast<BoidsInfos*>(sandBoxInfos.user);

	updateBoids(infos.boids, infos.vertices, infos.speed);
	infos.mesh->updateVertices(infos.vertices.data(), infos.vertices.size());

	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 0.f, 0.f, 0.f });
}

void boidsEnd(SandBoxInfos& sandBoxInfos)
{
	BoidsInfos& infos = *reinterpret_cast<BoidsInfos*>(sandBoxInfos.user);

	delete infos.mesh;
	delete sandBoxInfos.user;
}
