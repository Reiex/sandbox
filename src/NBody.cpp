#include <SandBox/SandBox.hpp>

struct Particle
{
	scp::f64vec3 p;
	scp::f64vec3 v;
	scp::f64vec3 a;
	double mass;
};

struct TreeNode
{
	std::vector<TreeNode> childs;
	scp::f64vec3 center;
	double mass;

	Particle* particle;
};

void computeOctree(TreeNode& node, Particle* particles, uint64_t count, scp::f64vec4& rect)
{
	// rect = { cx, cy, cz, size }

	if (count == 0)
	{
		node.childs.clear();
		node.center = { 0.0, 0.0, 0.0 };
		node.mass = 0.0;
		node.particle = nullptr;
	}
	else if (count == 1)
	{
		node.childs.clear();
		node.center = particles->p;
		node.mass = particles->mass;
		node.particle = particles;
	}
	else
	{
		node.childs.resize(8);
		node.center = { 0.0, 0.0, 0.0 };
		node.mass = 0.0;
		for (uint64_t i = 0; i < count; ++i)
		{
			node.center += particles[i].p;
			node.mass += particles[i].mass;
		}
		node.center /= count;
		node.particle = nullptr;

		uint64_t i, j;
		rect.w /= 2;

		// Octant (-1, -1, -1)

		i = 0;
		while (i < count && particles[i].p.x <= rect.x && particles[i].p.y <= rect.y && particles[i].p.z <= rect.z) ++i;
		j = count - 1;
		for (; j > i; --j)
		{
			if (particles[j].p.x <= rect.x && particles[j].p.y <= rect.y && particles[j].p.z <= rect.z)
			{
				std::swap(particles[i], particles[j]);
				while (i < j && particles[i].p.x <= rect.x && particles[i].p.y <= rect.y && particles[i].p.z <= rect.z) ++i;
			}
		}

		rect.x -= rect.w / 2.0;
		rect.y -= rect.w / 2.0;
		rect.z -= rect.w / 2.0;
		computeOctree(node.childs[0], particles, i, rect);
		particles += i;
		count -= i;
		rect.x += rect.w / 2.0;
		rect.y += rect.w / 2.0;
		rect.z += rect.w / 2.0;

		// Octant (1, -1, -1)

		i = 0;
		while (i < count && particles[i].p.x > rect.x && particles[i].p.y <= rect.y && particles[i].p.z <= rect.z) ++i;
		j = count ? count - 1 : 0;
		for (; j > i; --j)
		{
			if (particles[j].p.x > rect.x && particles[j].p.y <= rect.y && particles[j].p.z <= rect.z)
			{
				std::swap(particles[i], particles[j]);
				while (i < j && particles[i].p.x > rect.x && particles[i].p.y <= rect.y && particles[i].p.z <= rect.z) ++i;
			}
		}

		rect.x += rect.w / 2.0;
		rect.y -= rect.w / 2.0;
		rect.z -= rect.w / 2.0;
		computeOctree(node.childs[1], particles, i, rect);
		particles += i;
		count -= i;
		rect.x -= rect.w / 2.0;
		rect.y += rect.w / 2.0;
		rect.z += rect.w / 2.0;

		// Octant (-1, 1, -1)

		i = 0;
		while (i < count && particles[i].p.x <= rect.x && particles[i].p.y > rect.y && particles[i].p.z <= rect.z) ++i;
		j = count ? count - 1 : 0;
		for (; j > i; --j)
		{
			if (particles[j].p.x <= rect.x && particles[j].p.y > rect.y && particles[j].p.z <= rect.z)
			{
				std::swap(particles[i], particles[j]);
				while (i < j && particles[i].p.x <= rect.x && particles[i].p.y > rect.y && particles[i].p.z <= rect.z) ++i;
			}
		}

		rect.x -= rect.w / 2.0;
		rect.y += rect.w / 2.0;
		rect.z -= rect.w / 2.0;
		computeOctree(node.childs[2], particles, i, rect);
		particles += i;
		count -= i;
		rect.x += rect.w / 2.0;
		rect.y -= rect.w / 2.0;
		rect.z += rect.w / 2.0;

		// Octant (1, 1, -1)

		i = 0;
		while (i < count && particles[i].p.x > rect.x && particles[i].p.y > rect.y && particles[i].p.z <= rect.z) ++i;
		j = count ? count - 1 : 0;
		for (; j > i; --j)
		{
			if (particles[j].p.x > rect.x && particles[j].p.y > rect.y && particles[j].p.z <= rect.z)
			{
				std::swap(particles[i], particles[j]);
				while (i < j && particles[i].p.x > rect.x && particles[i].p.y > rect.y && particles[i].p.z <= rect.z) ++i;
			}
		}

		rect.x += rect.w / 2.0;
		rect.y += rect.w / 2.0;
		rect.z -= rect.w / 2.0;
		computeOctree(node.childs[3], particles, i, rect);
		particles += i;
		count -= i;
		rect.x -= rect.w / 2.0;
		rect.y -= rect.w / 2.0;
		rect.z += rect.w / 2.0;

		// Octant (-1, -1, 1)

		i = 0;
		while (i < count && particles[i].p.x <= rect.x && particles[i].p.y <= rect.y && particles[i].p.z > rect.z) ++i;
		j = count ? count - 1 : 0;
		for (; j > i; --j)
		{
			if (particles[j].p.x <= rect.x && particles[j].p.y <= rect.y && particles[j].p.z > rect.z)
			{
				std::swap(particles[i], particles[j]);
				while (i < j && particles[i].p.x <= rect.x && particles[i].p.y <= rect.y && particles[i].p.z > rect.z) ++i;
			}
		}

		rect.x -= rect.w / 2.0;
		rect.y -= rect.w / 2.0;
		rect.z += rect.w / 2.0;
		computeOctree(node.childs[4], particles, i, rect);
		particles += i;
		count -= i;
		rect.x += rect.w / 2.0;
		rect.y += rect.w / 2.0;
		rect.z -= rect.w / 2.0;

		// Octant (1, -1, 1)

		i = 0;
		while (i < count && particles[i].p.x > rect.x && particles[i].p.y <= rect.y && particles[i].p.z > rect.z) ++i;
		j = count ? count - 1 : 0;
		for (; j > i; --j)
		{
			if (particles[j].p.x > rect.x && particles[j].p.y <= rect.y && particles[j].p.z > rect.z)
			{
				std::swap(particles[i], particles[j]);
				while (i < j && particles[i].p.x > rect.x && particles[i].p.y <= rect.y && particles[i].p.z > rect.z) ++i;
			}
		}

		rect.x += rect.w / 2.0;
		rect.y -= rect.w / 2.0;
		rect.z += rect.w / 2.0;
		computeOctree(node.childs[5], particles, i, rect);
		particles += i;
		count -= i;
		rect.x -= rect.w / 2.0;
		rect.y += rect.w / 2.0;
		rect.z -= rect.w / 2.0;

		// Octant (-1, 1, 1)

		i = 0;
		while (i < count && particles[i].p.x <= rect.x && particles[i].p.y > rect.y && particles[i].p.z > rect.z) ++i;
		j = count ? count - 1 : 0;
		for (; j > i; --j)
		{
			if (particles[j].p.x <= rect.x && particles[j].p.y > rect.y && particles[j].p.z > rect.z)
			{
				std::swap(particles[i], particles[j]);
				while (i < j && particles[i].p.x <= rect.x && particles[i].p.y > rect.y && particles[i].p.z > rect.z) ++i;
			}
		}

		rect.x -= rect.w / 2.0;
		rect.y += rect.w / 2.0;
		rect.z += rect.w / 2.0;
		computeOctree(node.childs[6], particles, i, rect);
		particles += i;
		count -= i;
		rect.x += rect.w / 2.0;
		rect.y -= rect.w / 2.0;
		rect.z -= rect.w / 2.0;

		// Octant (1, 1, 1)

		i = 0;
		while (i < count && particles[i].p.x > rect.x && particles[i].p.y > rect.y && particles[i].p.z > rect.z) ++i;
		j = count ? count - 1 : 0;
		for (; j > i; --j)
		{
			if (particles[j].p.x > rect.x && particles[j].p.y > rect.y && particles[j].p.z > rect.z)
			{
				std::swap(particles[i], particles[j]);
				while (i < j && particles[i].p.x > rect.x && particles[i].p.y > rect.y && particles[i].p.z > rect.z) ++i;
			}
		}

		rect.x += rect.w / 2.0;
		rect.y += rect.w / 2.0;
		rect.z += rect.w / 2.0;
		computeOctree(node.childs[7], particles, i, rect);
		particles += i;
		count -= i;
		rect.x -= rect.w / 2.0;
		rect.y -= rect.w / 2.0;
		rect.z -= rect.w / 2.0;


		rect.w *= 2.0;
	}
}

void computeAcceleration(Particle& particle, TreeNode& node, double nodeSize)
{
	constexpr double G = 6.67430e-11;
	constexpr double theta = 2.0;

	if (node.childs.empty() && !node.particle)
	{
		return;
	}

	double d = scp::distance(particle.p, node.center);
	if (node.particle || nodeSize / d < theta)
	{
		if (&particle == node.particle)
		{
			return;
		}

		const double d3 = d * d * d;
		particle.a += (G * node.mass * (node.center - particle.p) / d3) * 1e-2;
	}
	else
	{
		nodeSize /= 2;
		for (TreeNode& child : node.childs)
		{
			computeAcceleration(particle, child, nodeSize);
		}
		nodeSize *= 2;
	}
}

void computeMovement(std::vector<Particle>& particles, TreeNode& root, double dt)
{
	scp::f64vec4 rect = { 0.0, 0.0, 0.0, 0.0 };

	for (const Particle& particle : particles)
	{
		if (particle.p.x < rect.x)
		{
			rect.w += rect.x - particle.p.x;
			rect.x = particle.p.x;
		}
		else if (particle.p.x > rect.x + rect.w)
		{
			rect.w = particle.p.x - rect.x;
		}

		if (particle.p.y < rect.y)
		{
			rect.w += rect.y - particle.p.y;
			rect.y = particle.p.y;
		}
		else if (particle.p.y > rect.y + rect.w)
		{
			rect.w = particle.p.y - rect.y;
		}

		if (particle.p.z < rect.z)
		{
			rect.w += rect.z - particle.p.z;
			rect.z = particle.p.z;
		}
		else if (particle.p.z > rect.z + rect.w)
		{
			rect.w = particle.p.z - rect.z;
		}
	}

	rect.x += rect.w / 2;
	rect.y += rect.w / 2;
	rect.z += rect.w / 2;

	computeOctree(root, particles.data(), particles.size(), rect);

	#pragma omp parallel for
	for (int64_t i = 0; i < particles.size(); ++i)
	{
		particles[i].a = { 0.0, 0.0, 0.0 };
		computeAcceleration(particles[i], root, rect.w);
		particles[i].v += particles[i].a * dt;
		particles[i].p += particles[i].v * dt;
	}
}

struct NBodyInfos
{
	static constexpr uint64_t n = 10000;

	TreeNode root;
	std::vector<Particle> particles = std::vector<Particle>(n);
	std::vector<lys::VertexDefaultMesh> vertices = std::vector<lys::VertexDefaultMesh>(n);

	lys::Mesh<>* mesh;

	double speed = 10.0;
};

void nBodyInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new NBodyInfos();

	NBodyInfos& infos = *reinterpret_cast<NBodyInfos*>(sandBoxInfos.user);

	for (Particle& particle : infos.particles)
	{
		particle.mass = random();
		particle.mass = 0.1 + std::pow(particle.mass, 2);
		particle.p.x = random(-1.f, 1.f);
		particle.p.y = random(-1.f, 1.f);
		particle.p.z = random(-1.f, 1.f);
		particle.v = { 0.0, 0.0, 0.0 };
	}

	for (uint64_t i = 0; i < infos.n; ++i)
	{
		infos.vertices[i].position.x = infos.particles[i].p.x;
		infos.vertices[i].position.y = infos.particles[i].p.y;
		infos.vertices[i].position.z = infos.particles[i].p.z;
	}

	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferUsage::StreamDraw, spl::BufferStorageFlags::None);
	// infos.mesh->setPrimitiveType(spl::PrimitiveType::Points);
	infos.mesh->setScale(10.f);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void nBodyUpdate(SandBoxInfos& sandBoxInfos)
{
	NBodyInfos& infos = *reinterpret_cast<NBodyInfos*>(sandBoxInfos.user);

	for (uint64_t i = 0; i < 5; ++i)
	{
		computeMovement(infos.particles, infos.root, infos.speed);
	}
	for (uint64_t i = 0; i < infos.n; ++i)
	{
		infos.vertices[i].position.x = infos.particles[i].p.x;
		infos.vertices[i].position.y = infos.particles[i].p.y;
		infos.vertices[i].position.z = infos.particles[i].p.z;
	}
	infos.mesh->updateVertices(infos.vertices.data(), infos.vertices.size());

	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 0.f, 0.f, 0.f });
}

void nBodyEnd(SandBoxInfos& sandBoxInfos)
{
	NBodyInfos& infos = *reinterpret_cast<NBodyInfos*>(sandBoxInfos.user);

	delete infos.mesh;
	delete sandBoxInfos.user;
}
