#include <SandBox/SandBox.hpp>

struct Drop
{
	scp::f32vec2 pos;
	scp::f32vec2 dir;
	float vel;
	float water;
	float sediment;
};

void createRandomHeightMap(uint64_t n, std::vector<lys::VertexDefaultMesh>& vertices, scp::Matrix<scp::f32vec3>& pos)
{
	for (uint64_t i = 0; i < n*n; ++i)
	{
		pos[i] = vertices[i].position.xyz();
		pos[i].y += 1.f;
	}

	srand(0);

	scp::Matrix<float> grid(5, 5);
	for (uint64_t i = 0; i < grid.getSize(0); ++i)
	{
		for (uint64_t j = 0; j < grid.getSize(1); ++j)
		{
			if (j == grid.getSize(1) - 1)
			{
				grid[{i, j}] = grid[{i, 0}];
			}
			else if (i == grid.getSize(0) - 1)
			{
				grid[{i, j}] = grid[{0, j}];
			}
			else
			{
				grid[{i, j}] = random();
			}
		}
	}

	float step = static_cast<float>(grid.getSize(0)) / n;
	while (step < 1.f)
	{
		float y0 = random();

		float x = random();
		for (uint64_t i = 0; i < n; ++i, x += step)
		{
			if (x >= grid.getSize(0) - 1)
			{
				x -= grid.getSize(0) - 1;
			}

			float y = y0;
			for (uint64_t j = 0; j < n; ++j, y += step)
			{
				if (y >= grid.getSize(1) - 1)
				{
					y -= grid.getSize(1) - 1;
				}

				pos[{i, j}].y += grid.getInterpolated<float, scp::InterpolationMethod::Linear>({ x, y }) / step;
			}
		}

		step *= 1.9f;
	}

	float ratio = 1.f / pos.maxElement([&](const scp::f32vec3& x, const scp::f32vec3& y) { return x.y < y.y; }).y;
	for (uint64_t i = 0; i < pos.getElementCount(); ++i)
	{
		pos[i].y = std::sqrt(pos[i].y * ratio);
		// pos[i].y = pos[i].y * ratio;
	}
}

void erosionDrop(scp::Matrix<scp::f32vec3>& pos)
{
	constexpr scp::BorderBehaviour oob = scp::BorderBehaviour::Continuous;

	constexpr float pInertia = 0.001f;
	constexpr float pCapacity = 10.f;
	constexpr float pMinSlope = 0.01f;
	constexpr float pDeposition = 0.01f;
	constexpr float pErosion = 0.2f;
	constexpr float pGravity = 9.81f;
	constexpr float pEvaporation = 0.01f;
	constexpr uint64_t pRadius = 3;

	Drop drop;
	drop.pos = { random(0.f, pos.getSize(0) - 1.f), random(0.f, pos.getSize(1) - 1.f) };
	drop.dir = scp::normalize(scp::f32vec2{ random(-1.f, 1.f), random(-1.f, 1.f) });
	drop.vel = 0.f;
	drop.water = 0.01f;
	drop.sediment = 0.f;

	uint64_t iter = 0;
	while (iter < pos.getSize(0))
	{
		++iter;

		// Compute bound points and gradient

		const int64_t p00[2] = { static_cast<int64_t>(drop.pos.x), static_cast<int64_t>(drop.pos.y) };
		const int64_t p10[2] = { p00[0] + 1, p00[1] };
		const int64_t p01[2] = { p00[0], p00[1] + 1 };
		const int64_t p11[2] = { p00[0] + 1, p00[1] + 1 };

		const uint64_t up00[2] = { p00[0], p00[1] };
		const uint64_t up10[2] = { std::min<uint64_t>(p10[0], pos.getSize(0) - 1), p10[1] };
		const uint64_t up01[2] = { p01[0], std::min<uint64_t>(p01[1], pos.getSize(1) - 1) };
		const uint64_t up11[2] = { std::min<uint64_t>(p11[0], pos.getSize(0) - 1), std::min<uint64_t>(p11[1], pos.getSize(1) - 1) };

		const float u = drop.pos.x - p00[0];
		const float v = drop.pos.y - p00[1];

		const scp::f32vec2 g = {
			(pos.getOutOfBound<oob>(p10).y - pos.getOutOfBound<oob>(p00).y) * (1.f - v) + (pos.getOutOfBound<oob>(p11).y - pos.getOutOfBound<oob>(p01).y) * v,
			(pos.getOutOfBound<oob>(p01).y - pos.getOutOfBound<oob>(p00).y) * (1.f - u) + (pos.getOutOfBound<oob>(p11).y - pos.getOutOfBound<oob>(p10).y) * u,
		};

		// Compute new drop dir and new drop pos

		drop.dir = drop.dir * pInertia - g * (1.f - pInertia);
		if (drop.dir.x == 0.f && drop.dir.y == 0.f)
		{
			drop.dir = scp::normalize(scp::f32vec2{ random(-1.f, 1.f), random(-1.f, 1.f) });
		}
		else
		{
			drop.dir = scp::normalize(drop.dir);
		}

		scp::f32vec2 posNew = drop.pos + drop.dir;
		if (posNew.x < 0 || posNew.x > pos.getSize(0) - 1 || posNew.y < 0 || posNew.y > pos.getSize(1) - 1)
		{
			pos[{ up00[0], up00[1] }].y += drop.sediment * (1.f - u) * (1.f - v);
			pos[{ up10[0], up10[1] }].y += drop.sediment * u * (1.f - v);
			pos[{ up01[0], up01[1] }].y += drop.sediment * (1.f - u) * v;
			pos[{ up11[0], up11[1] }].y += drop.sediment * u * v;

			break;
		}
		
		// Compute erosion

		const float hOld = pos.getInterpolated<float, scp::InterpolationMethod::Linear>({ drop.pos.x, drop.pos.y }).y;
		const float hNew = pos.getInterpolated<float, scp::InterpolationMethod::Linear>({ posNew.x, posNew.y }).y;
		const float hDif = hNew - hOld;

		if (hDif > 0.f)
		{
			pos[{ up00[0], up00[1] }].y += drop.sediment * (1.f - u) * (1.f - v);
			pos[{ up10[0], up10[1] }].y += drop.sediment * u * (1.f - v);
			pos[{ up01[0], up01[1] }].y += drop.sediment * (1.f - u) * v;
			pos[{ up11[0], up11[1] }].y += drop.sediment * u * v;

			break;
		}

		const float c = std::max(-hDif, pMinSlope) * drop.vel * drop.water * pCapacity;
		if (c > drop.sediment)
		{
			const float picked = std::min(c - drop.sediment * pErosion, -hDif);

			const uint64_t iMin = std::max<int64_t>(p00[0] - (pRadius + 1), 0);
			const uint64_t iMax = std::min<int64_t>(p00[0] + (pRadius + 1), pos.getSize(0));
			const uint64_t jMin = std::max<int64_t>(p00[1] - (pRadius + 1), 0);
			const uint64_t jMax = std::min<int64_t>(p00[1] + (pRadius + 1), pos.getSize(1));

			const scp::f32vec2 dropPos = { 2.f * drop.pos.x / (pos.getSize(0)) - 1.f, 2.f * drop.pos.y / (pos.getSize(1)) - 1.f };

			float ratio = 0.f;
			for (uint64_t i = iMin; i < iMax; ++i)
			{
				for (uint64_t j = jMin; j < jMax; ++j)
				{
					const scp::f32vec2 p = { pos[{i, j}].x, pos[{i, j}].z };
					ratio += std::max<float>(0, pRadius - scp::distance(p, dropPos));
				}
			}

			for (uint64_t i = iMin; i < iMax; ++i)
			{
				for (uint64_t j = jMin; j < jMax; ++j)
				{
					const scp::f32vec2 p = { pos[{i, j}].x, pos[{i, j}].z };
					pos[{i, j}].y -= picked * std::max<float>(0, pRadius - scp::distance(p, dropPos)) / ratio;
				}
			}

			drop.sediment += picked;
		}
		else
		{
			const float dropped = (drop.sediment - c) * pDeposition;

			pos[{ up00[0], up00[1] }].y += dropped * (1.f - u) * (1.f - v);
			pos[{ up10[0], up10[1] }].y += dropped * u * (1.f - v);
			pos[{ up01[0], up01[1] }].y += dropped * (1.f - u) * v;
			pos[{ up11[0], up11[1] }].y += dropped * u * v;

			drop.sediment -= dropped;
		}

		// Compute new drop params
		
		drop.pos = posNew;
		drop.vel = std::sqrt(drop.vel * drop.vel - hDif * pGravity);
		drop.water *= 1.f - pEvaporation;
	}
}

struct ErosionInfos
{
	static constexpr uint32_t n = 768;
	scp::Matrix<scp::f32vec3> pos = {n, n};

	std::vector<lys::VertexDefaultMesh> vertices;
	std::vector<uint32_t> indices;

	lys::Material* material;
	lys::Mesh<>* mesh;
	lys::Mesh<>* meshBefore;
};

void erosionInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new ErosionInfos();

	ErosionInfos& infos = *reinterpret_cast<ErosionInfos*>(sandBoxInfos.user);

	createVertexGrid(infos.n, infos.vertices, infos.indices);
	createRandomHeightMap(infos.n, infos.vertices, infos.pos);
	updateVertexGrid(infos.n, infos.vertices, infos.pos.getData());

	infos.material = new lys::Material(0.5f, 0.4f, 0.4f, 1.f, 0.2f, 0.1f, 0.8f);

	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferUsage::StreamDraw, infos.indices.data(), infos.indices.size(), spl::BufferStorageFlags::None);
	infos.meshBefore = new lys::Mesh<>(*infos.mesh);

	infos.mesh->setMaterial(infos.material);
	infos.meshBefore->setMaterial(infos.material);

	infos.mesh->move(1.f, 0.f, 0.f);
	infos.meshBefore->move(-1.f, 0.f, 0.f);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.scene->addDrawable(infos.meshBefore);

	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void erosionUpdate(SandBoxInfos& sandBoxInfos)
{
	ErosionInfos& infos = *reinterpret_cast<ErosionInfos*>(sandBoxInfos.user);

	#pragma omp parallel for
	for (int32_t i = 0; i < 10000; ++i)
	{
		erosionDrop(infos.pos);
	}
	updateVertexGrid(infos.n, infos.vertices, infos.pos.getData());
	infos.mesh->updateVertices(infos.vertices.data(), infos.vertices.size());

	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 1.f, 0.f, 0.f });
}

void erosionEnd(SandBoxInfos& sandBoxInfos)
{
	ErosionInfos& infos = *reinterpret_cast<ErosionInfos*>(sandBoxInfos.user);

	delete infos.material;
	delete infos.mesh;
	delete infos.meshBefore;
	delete sandBoxInfos.user;
}
