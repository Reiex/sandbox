#include <SandBox/SandBox.hpp>

struct GerstnerParams
{
	scp::f32vec2 k;

	float amplitude;
	scp::f32vec2 amplitudeDir;

	float pulsation;
	float phase;
};

void createGerstnerWaves(uint64_t n, float size, float duration, std::vector<GerstnerParams>& params)
{
	constexpr float g = 9.81f;

	const float omega0 = 2.f * std::numbers::pi / duration;

	srand(2);

	params.clear();
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j)
		{
			if (i < 3 && j < 3)
			{
				if (rand() % 2 != 0)
				{
					continue;
				}
			}
			else if (rand() % (i*j + n) != 0)
			{
				continue;
			}

			scp::f32vec2 lambda = { size / (i + 1), size / (j + 1) };
			lambda.x = 2.f * std::numbers::pi / lambda.x;
			lambda.y = 2.f * std::numbers::pi / lambda.y;
			float k = scp::length(lambda);

			GerstnerParams param;
			param.k = lambda;
			param.amplitude = 0.6 / k;
			param.amplitudeDir = (lambda / k) * param.amplitude;
			param.pulsation = std::round(std::sqrt(g * k) / omega0) * omega0;
			param.phase = random();

			params.push_back(param);
		}
	}

	for (GerstnerParams& param : params)
	{
		param.amplitude /= std::sqrt(params.size());
		param.amplitudeDir /= std::sqrt(params.size());
	}
}

void updateGerstnerWaves(float t, std::vector<scp::f32vec3>& pos, float size, const std::vector<GerstnerParams>& params)
{
	const uint64_t n = std::sqrt(pos.size());

	const float step = size / (n - 1);
	const float ratio = 2.f / size;

	auto it = pos.begin();
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j, ++it)
		{
			const float x0 = i * step, y0 = j * step;
			it->x = x0;
			it->z = y0;
			it->y = 0;

			for (const GerstnerParams& param : params)
			{
				const float phi = scp::dot(param.k, { x0, y0 }) - param.pulsation * t + param.phase;
				const float sPhi = std::sin(phi);
				const float cPhi = std::cos(phi);

				it->x -= param.amplitudeDir.x * sPhi;
				it->z -= param.amplitudeDir.y * sPhi;
				it->y += param.amplitude * cPhi;
			}
			
			it->x = it->x * ratio - 1.f;
			it->z = it->z * ratio - 1.f;
			it->y = it->y * ratio;
		}
	}
}

struct GerstnerWavesInfos
{
	static constexpr uint32_t n = 128;

	std::vector<scp::f32vec3> pos = std::vector<scp::f32vec3>(n * n);

	std::vector<lys::VertexDefaultMesh> vertices;
	std::vector<uint32_t> indices;

	lys::Material* material;
	lys::Mesh<>* mesh;

	static constexpr float size = 100.f;
	static constexpr float duration = 60.f;

	std::vector<GerstnerParams> params;

	double time = 0.0;
};

void gertsnerWavesInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new GerstnerWavesInfos();

	GerstnerWavesInfos& infos = *reinterpret_cast<GerstnerWavesInfos*>(sandBoxInfos.user);

	createVertexGrid(infos.n, infos.vertices, infos.indices);

	infos.material = new lys::Material(0.2f, 0.4f, 1.f, 1.f, 0.2f, 0.5f, 0.1f);
	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferUsage::StreamDraw, infos.indices.data(), infos.indices.size(), spl::BufferStorageFlags::None);
	infos.mesh->setMaterial(infos.material);

	createGerstnerWaves(infos.n, infos.size, infos.duration, infos.params);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void gerstnerWavesUpdate(SandBoxInfos& sandBoxInfos)
{
	GerstnerWavesInfos& infos = *reinterpret_cast<GerstnerWavesInfos*>(sandBoxInfos.user);
	
	infos.time += sandBoxInfos.dt;
	if (infos.time > infos.duration)
	{
		infos.time -= infos.duration;
	}
	
	updateGerstnerWaves(infos.time, infos.pos, infos.size, infos.params);
	updateVertexGrid(infos.n, infos.vertices, infos.pos.data());
	infos.mesh->updateVertices(infos.vertices.data(), infos.vertices.size());

	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 0.f, 0.f, 0.f });
}

void gerstnerWavesEnd(SandBoxInfos& sandBoxInfos)
{
	GerstnerWavesInfos& infos = *reinterpret_cast<GerstnerWavesInfos*>(sandBoxInfos.user);

	delete infos.material;
	delete infos.mesh;
	delete sandBoxInfos.user;
}



void createPhillipsWaves(int64_t n, float size, scp::Matrix<std::complex<float>>& h0)
{
	constexpr float g = 9.81f;

	const scp::f32vec2 wind = { 10.f, 10.f };
	const float VSq = wind.x * wind.x + wind.y * wind.y;
	const float LSq = VSq * VSq / (g * g);

	std::normal_distribution<float> distrib;
	std::minstd_rand generator;

	uint64_t index = 0;
	for (int64_t i = 0; i < n; ++i)
	{
		for (int64_t j = 0; j < n; ++j, ++index)
		{
			scp::f32vec2 k = 2.f * std::numbers::pi * scp::f32vec2(1, 1) / size;
			if (i > n / 2)
			{
				if (j > n / 2)
				{
					k *= scp::f32vec2(i - n, j - n);
				}
				else
				{
					k *= scp::f32vec2(i - n, j);
				}
			}
			else
			{
				if (j > n / 2)
				{
					k *= scp::f32vec2(i, j - n);
				}
				else
				{
					k *= scp::f32vec2(i, j);
				}
			}

			const float kSq = k.x * k.x + k.y * k.y;
			const float p = kSq ? (pow(k.x * wind.x + k.y * wind.y, 2) / (kSq * VSq)) * std::exp(-1.f / (LSq * kSq)) / (kSq * kSq) : 0;
			h0[index] = std::complex<float>(distrib(generator), distrib(generator)) * std::sqrt(p / 2.f);
		}
	}
}

void updatePhillipsWaves(float t, std::vector<scp::f32vec3>& pos, float size, const scp::Matrix<std::complex<float>>& h0)
{
	constexpr float g = 9.81f;

	const int64_t n = h0.getSize(0);
	const float step = size / (n - 1);

	scp::Matrix<std::complex<float>> h(n, n), dx(n, n), dy(n, n);

	uint64_t index = 0;
	for (int64_t i = 0; i < n; ++i)
	{
		for (int64_t j = 0; j < n; ++j, ++index)
		{
			scp::f32vec2 k = 2.f * std::numbers::pi * scp::f32vec2(1, 1) / size;
			if (i > n / 2)
			{
				if (j > n / 2)
				{
					k *= scp::f32vec2(i - n, j - n);
				}
				else
				{
					k *= scp::f32vec2(i - n, j);
				}
			}
			else
			{
				if (j > n / 2)
				{
					k *= scp::f32vec2(i, j - n);
				}
				else
				{
					k *= scp::f32vec2(i, j);
				}
			}
			const float kLength = scp::length(k);

			const float omega = std::sqrt(g * scp::length(k));
			const std::complex<float> a = std::exp(std::complex<float>(0, omega * t));
			h[index] = h0[index] * a + std::conj(h0[{static_cast<uint64_t>(n - i - 1), static_cast<uint64_t>(n - j - 1)}]) / a;

			if (i != 0 || j != 0)
			{
				dx[index] = std::complex<float>(0, -k.x / kLength) * h[index];
				dy[index] = std::complex<float>(0, -k.y / kLength) * h[index];
			}
			else
			{
				dx[index] = 0;
				dy[index] = 0;
			}
		}
	}

	h.ifft();
	dx.ifft();
	dy.ifft();

	constexpr float lambda = 1.f;

	index = 0;
	for (uint64_t i = 0; i < n; ++i)
	{
		for (uint64_t j = 0; j < n; ++j, ++index)
		{
			// pos[index] = {2.f * j / (n - 1) - 1.f, std::abs(h[index]) * lambda, 2.f * i / (n - 1) - 1.f};
			pos[index] = { 2.f * i / (n - 1) - 1.f + std::abs(dy[index]) * lambda, -std::abs(h[index]) * lambda * 2.f + 0.5f, 2.f * j / (n - 1) - 1.f + std::abs(dx[index]) * lambda };
		}
	}
}

struct PhillipsWavesInfos
{
	static constexpr uint32_t n = 256;

	std::vector<scp::f32vec3> pos = std::vector<scp::f32vec3>(n * n);

	std::vector<lys::VertexDefaultMesh> vertices;
	std::vector<uint32_t> indices;

	lys::Material* material;
	lys::Mesh<>* mesh;

	static constexpr float size = 150.f;

	scp::Matrix<std::complex<float>> h0 = { n, n };

	double time = 0.0;
};

void phillipsWavesInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new PhillipsWavesInfos();

	PhillipsWavesInfos& infos = *reinterpret_cast<PhillipsWavesInfos*>(sandBoxInfos.user);

	createVertexGrid(infos.n, infos.vertices, infos.indices);

	infos.material = new lys::Material(0.2f, 0.4f, 1.f, 1.f, 0.2f, 0.5f, 0.1f);
	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferUsage::StreamDraw, infos.indices.data(), infos.indices.size(), spl::BufferStorageFlags::None);
	infos.mesh->setMaterial(infos.material);

	createPhillipsWaves(infos.n, infos.size, infos.h0);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void phillipsWavesUpdate(SandBoxInfos& sandBoxInfos)
{
	PhillipsWavesInfos& infos = *reinterpret_cast<PhillipsWavesInfos*>(sandBoxInfos.user);

	infos.time += sandBoxInfos.dt;

	updatePhillipsWaves(infos.time, infos.pos, infos.size, infos.h0);
	updateVertexGrid(infos.n, infos.vertices, infos.pos.data());
	infos.mesh->updateVertices(infos.vertices.data(), infos.vertices.size());

	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 0.f, 0.f, 0.f });
}

void phillipsWavesEnd(SandBoxInfos& sandBoxInfos)
{
	PhillipsWavesInfos& infos = *reinterpret_cast<PhillipsWavesInfos*>(sandBoxInfos.user);

	delete infos.material;
	delete infos.mesh;
	delete sandBoxInfos.user;
}
