#include <main.hpp>

int main()
{
	SandBox gerstnerWaves("Gerstner Waves", &gertsnerWavesInit, &gerstnerWavesUpdate, &gerstnerWavesEnd);
	SandBox phillipsWaves("Phillips Waves", &phillipsWavesInit, &phillipsWavesUpdate, &phillipsWavesEnd);
	SandBox lorenzAttractor("Lorenz Attractor", &lorenzAttractorInit, &lorenzAttractorUpdate, &lorenzAttractorEnd);
	SandBox rosslerAttractor("Rossler Attractor", &rosslerAttractorInit, &rosslerAttractorUpdate, &rosslerAttractorEnd);
	SandBox lorenzPath("Lorenz Path", &lorenzPathInit, &lorenzPathUpdate, &lorenzPathEnd);
	SandBox lorenzPoints("Lorenz Points", &lorenzPointsInit, &lorenzPointsUpdate, &lorenzPointsEnd);
	SandBox nBody("N-Body", &nBodyInit, &nBodyUpdate, &nBodyEnd);
	SandBox boids("Boids", &boidsInit, &boidsUpdate, &boidsEnd);
	SandBox erosion("Erosion", &erosionInit, &erosionUpdate, &erosionEnd);

	gerstnerWaves.run();
	phillipsWaves.run();
	// // lorenzAttractor.run();
	// // rosslerAttractor.run();
	// // lorenzPath.run();
	// // lorenzPoints.run();
	// // nBody.run();
	// // boids.run();
	erosion.run();
	mainSphereRaytracing();

	/*
	
	Fire simulation ?

	*/

	return 0;
}
