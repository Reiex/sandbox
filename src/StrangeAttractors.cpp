#include <SandBox/SandBox.hpp>

void computeLorenz(std::vector<lys::VertexDefaultMesh>& vertices, float t)
{
	constexpr float sigma = 10.f;
	constexpr float rho = 28.f;
	constexpr float beta = 8.f / 3.f;

	const float dt = t / vertices.size();

	auto itPreced = vertices.begin();
	auto it = itPreced + 1;
	const auto itEnd = vertices.cend();

	for (uint64_t i = 0; it != itEnd; ++itPreced, ++it, ++i)
	{
		const scp::f32vec4& pos = itPreced->position;
		it->position = pos;
		it->position.x += dt * sigma * (pos.y - pos.x);
		it->position.y += dt * (rho * pos.x - pos.y - pos.x * pos.z);
		it->position.z += dt * (pos.x * pos.y - beta * pos.z);

		float ratio = i * dt / t;
		if (ratio < 0.5f)
		{
			ratio *= 2.f;
			it->normal = { ratio, 0.f, 0.f, 0.f };
		}
		else
		{
			ratio = (ratio - 0.5f) * 2.f;
			it->normal = { 1.f, ratio, ratio, 0.f };
		}
	}
}

struct LorenzAttractorInfos
{
	std::vector<lys::VertexDefaultMesh> vertices = std::vector<lys::VertexDefaultMesh>(1 << 18);
	
	lys::Mesh<>* mesh;
};

void lorenzAttractorInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new LorenzAttractorInfos();

	LorenzAttractorInfos& infos = *reinterpret_cast<LorenzAttractorInfos*>(sandBoxInfos.user);

	infos.vertices[0].position = { 1.f, 1.f, 1.f, 1.f };
	computeLorenz(infos.vertices, 300.f);

	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferStorageFlags::None, spl::BufferStorageFlags::None);
	// infos.mesh->setPrimitiveType(spl::PrimitiveType::LineStrips);
	infos.mesh->scale(0.05f);
	infos.mesh->move(0.f, 0.f, -0.5f);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void lorenzAttractorUpdate(SandBoxInfos& sandBoxInfos)
{
	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 0.f, 0.f, 0.f });
}

void lorenzAttractorEnd(SandBoxInfos& sandBoxInfos)
{
	LorenzAttractorInfos& infos = *reinterpret_cast<LorenzAttractorInfos*>(sandBoxInfos.user);

	delete infos.mesh;
	delete sandBoxInfos.user;
}



void computeRossler(std::vector<lys::VertexDefaultMesh>& vertices, float t)
{
	constexpr float a = 0.2f;
	constexpr float b = 0.2f;
	constexpr float c = 5.7f;

	const float dt = t / vertices.size();

	auto itPreced = vertices.begin();
	auto it = itPreced + 1;
	const auto itEnd = vertices.cend();

	for (uint64_t i = 0; it != itEnd; ++itPreced, ++it, ++i)
	{
		const scp::f32vec4& pos = itPreced->position;
		it->position = pos;

		it->position.x += dt * (-pos.z - pos.y);
		it->position.z += dt * (pos.x + a * pos.z);
		it->position.y += dt * (b + pos.y * (pos.x - c));

		float ratio = i * dt / t;
		if (ratio < 0.5f)
		{
			ratio *= 2.f;
			it->normal = { ratio, 0.f, 0.f, 0.f };
		}
		else
		{
			ratio = (ratio - 0.5f) * 2.f;
			it->normal = { 1.f, ratio, ratio, 0.f };
		}
	}
}

struct RosslerAttractorInfos
{
	std::vector<lys::VertexDefaultMesh> vertices = std::vector<lys::VertexDefaultMesh>(1 << 18);

	lys::Mesh<>* mesh;
};

void rosslerAttractorInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new RosslerAttractorInfos();

	RosslerAttractorInfos& infos = *reinterpret_cast<RosslerAttractorInfos*>(sandBoxInfos.user);

	infos.vertices[0].position = { 1.f, 1.f, 1.f, 1.f };
	computeRossler(infos.vertices, 500.f);

	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferStorageFlags::None, spl::BufferStorageFlags::None);
	// infos.mesh->setPrimitiveType(spl::PrimitiveType::LineStrips);
	infos.mesh->scale(0.05f);
	infos.mesh->move(0.f, 0.f, -0.5f);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void rosslerAttractorUpdate(SandBoxInfos& sandBoxInfos)
{
	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 0.f, 0.f, 0.f });
}

void rosslerAttractorEnd(SandBoxInfos& sandBoxInfos)
{
	RosslerAttractorInfos& infos = *reinterpret_cast<RosslerAttractorInfos*>(sandBoxInfos.user);

	delete infos.mesh;
	delete sandBoxInfos.user;
}



void computeLorenzPath(std::vector<lys::VertexDefaultMesh>& vertices, std::vector<uint32_t>& indices, uint64_t& n)
{
	constexpr uint64_t m = 10;
	constexpr float dt = 5e-3f;

	constexpr float sigma = 10.f;
	constexpr float rho = 28.f;
	constexpr float beta = 8.f / 3.f;

	if (n + m > vertices.size())
	{
		vertices.resize(n + m);
		indices.resize(n + m);
	}

	auto it = vertices.begin() + n;
	auto itPreced = it - 1;
	auto itIndices = indices.begin() + n;
	const auto itEnd = vertices.cend();

	for (uint64_t i = 0; i < m; ++it, ++itPreced, ++itIndices, ++n, ++i)
	{
		const scp::f32vec4& pos = itPreced->position;
		it->position = pos;
		it->position.x += dt * sigma * (pos.y - pos.x);
		it->position.y += dt * (rho * pos.x - pos.y - pos.x * pos.z);
		it->position.z += dt * (pos.x * pos.y - beta * pos.z);

		*itIndices = n;
	}
}

struct LorenzPathInfos
{
	uint64_t n = 1;
	std::vector<lys::VertexDefaultMesh> vertices = { { {1.f, 1.f, 1.f, 1.f}, {0.f}, {0.f}, {0.f}} };
	std::vector<uint32_t> indices = { 0 };

	lys::Mesh<>* mesh;
};

void lorenzPathInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new LorenzPathInfos();

	LorenzPathInfos& infos = *reinterpret_cast<LorenzPathInfos*>(sandBoxInfos.user);

	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferUsage::StreamDraw, spl::BufferUsage::StreamDraw);
	// infos.mesh->setPrimitiveType(spl::PrimitiveType::LineStrips);
	infos.mesh->scale(0.05f);
	infos.mesh->move(0.f, 0.f, -0.5f);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void lorenzPathUpdate(SandBoxInfos& sandBoxInfos)
{
	LorenzPathInfos& infos = *reinterpret_cast<LorenzPathInfos*>(sandBoxInfos.user);

	computeLorenzPath(infos.vertices, infos.indices, infos.n);
	infos.mesh->createNewVertices(infos.vertices.data(), infos.vertices.size(), spl::BufferUsage::StreamDraw);
	infos.mesh->createNewIndices(infos.indices.data(), infos.indices.size(), spl::BufferUsage::StreamDraw);

	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 0.f, 0.f, 0.f });
}

void lorenzPathEnd(SandBoxInfos& sandBoxInfos)
{
	LorenzPathInfos& infos = *reinterpret_cast<LorenzPathInfos*>(sandBoxInfos.user);

	delete infos.mesh;
	delete sandBoxInfos.user;
}


void computeLorenzPoints(std::vector<lys::VertexDefaultMesh>& vertices, float dt)
{
	constexpr float sigma = 10.f;
	constexpr float rho = 24.3f;
	constexpr float beta = 8.f / 3.f;

	for (lys::VertexDefaultMesh& vertex : vertices)
	{
		const scp::f32vec4& pos = vertex.position;
		vertex.position.x += dt * sigma * (pos.y - pos.x);
		vertex.position.y += dt * (rho * pos.x - pos.y - pos.x * pos.z);
		vertex.position.z += dt * (pos.x * pos.y - beta * pos.z);
	}
}

struct LorenzPointsInfos
{
	std::vector<lys::VertexDefaultMesh> vertices = std::vector<lys::VertexDefaultMesh>(1 << 18);

	lys::Mesh<>* mesh;
};

void lorenzPointsInit(SandBoxInfos& sandBoxInfos)
{
	sandBoxInfos.user = new LorenzPointsInfos();

	LorenzPointsInfos& infos = *reinterpret_cast<LorenzPointsInfos*>(sandBoxInfos.user);

	for (lys::VertexDefaultMesh& vertex : infos.vertices)
	{
		vertex.position.x = random(-50.f, 50.f);
		vertex.position.y = random(-50.f, 50.f);
		vertex.position.z = random(-50.f, 50.f);
		vertex.normal = { 1.f, 1.f, 1.f, 0.f };
	}

	infos.mesh = new lys::Mesh<>(infos.vertices.data(), infos.vertices.size(), spl::BufferUsage::StreamDraw, spl::BufferStorageFlags::None);
	// infos.mesh->setPrimitiveType(spl::PrimitiveType::Points);
	infos.mesh->scale(0.05f);
	infos.mesh->move(-1.f, 0.f, 0.f);
	infos.mesh->rotate(0.f, 1.f, 0.f, std::numbers::pi / 3);

	sandBoxInfos.scene->addDrawable(infos.mesh);
	sandBoxInfos.camera->setTranslation(0.f, 0.f, 3.f);
}

void lorenzPointsUpdate(SandBoxInfos& sandBoxInfos)
{
	LorenzPointsInfos& infos = *reinterpret_cast<LorenzPointsInfos*>(sandBoxInfos.user);

	computeLorenzPoints(infos.vertices, 0.001f);
	infos.mesh->updateVertices(infos.vertices.data(), infos.vertices.size());

	// Handle camera

	const float speed = 1.f * sandBoxInfos.dt;
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::W)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::S)) sandBoxInfos.camera->move(sandBoxInfos.camera->getUpVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::A)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * -speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::D)) sandBoxInfos.camera->move(sandBoxInfos.camera->getRightVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::Space)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * speed);
	if (sandBoxInfos.window->isKeyPressed(spl::KeyboardKey::LeftShift)) sandBoxInfos.camera->move(sandBoxInfos.camera->getFrontVector() * -speed);

	sandBoxInfos.camera->lookAt({ 0.f, 0.f, 0.f });
}

void lorenzPointsEnd(SandBoxInfos& sandBoxInfos)
{
	LorenzPointsInfos& infos = *reinterpret_cast<LorenzPointsInfos*>(sandBoxInfos.user);

	delete infos.mesh;
	delete sandBoxInfos.user;
}
