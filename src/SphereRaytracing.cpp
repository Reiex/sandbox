#include <SandBox/SandBox.hpp>

void mainSphereRaytracing()
{
	// Open window and set context

	spl::Window window(1000, 600, "SphereRaytracing", true);
	spl::Context* context = window.getContext();
	spl::Context::setCurrentContext(context);
	context->setIsDepthTestEnabled(true);
	context->setClearColor(0.2f, 0.3f, 0.3f, 1.f);

	// Set scene (shader, camera, ...)

	spl::ShaderProgram shader("assets/shaders/SphereRaytracing.vert", "assets/shaders/SphereRaytracing.frag");
	spl::ShaderProgram::bind(&shader);

	lys::CameraPerspective camera(1000, 600, 0.1f, 100.f, 1.f);
	camera.setTranslation({ 0.f, 0.f, 3.f });

	scp::f32vec3 lightDir = -scp::normalize(scp::f32vec3{ 1.0, 1.0, 1.0 });

	// Create screen mesh

	lys::Mesh<> mesh(
		{
			{{-1.f, -1.f, 0.f, 1.f}, {0.f}, {0.f}, {0.f, 0.f, 0.f, 0.f}},
			{{1.f, -1.f, 0.f, 1.f}, {0.f}, {0.f}, {1.f, 0.f, 0.f, 0.f}},
			{{-1.f, 1.f, 0.f, 1.f}, {0.f}, {0.f}, {0.f, 1.f, 0.f, 0.f}},
			{{1.f, 1.f, 0.f, 1.f}, {0.f}, {0.f}, {1.f, 1.f, 0.f, 0.f}}
		},
		spl::BufferStorageFlags::None,
		{ 0, 1, 2, 2, 1, 3 },
		spl::BufferStorageFlags::None
	);

	// Load skybox

	// spl::Texture2D skybox("assets/images/cylindric_skybox2.png");

	djv::Image_rgb_u8 skyboxRight	("assets/images/skybox1_right.png");
	djv::Image_rgb_u8 skyboxLeft	("assets/images/skybox1_left.png");
	djv::Image_rgb_u8 skyboxTop		("assets/images/skybox1_top.png");
	djv::Image_rgb_u8 skyboxBottom	("assets/images/skybox1_bottom.png");
	djv::Image_rgb_u8 skyboxFront	("assets/images/skybox1_front.png");
	djv::Image_rgb_u8 skyboxBack	("assets/images/skybox1_back.png");
	
	spl::TextureCreationParams createParams;
	createParams.target = spl::TextureTarget::CubeMap;
	createParams.internalFormat = spl::TextureInternalFormat::RGB_nu8;
	createParams.width = skyboxFront.getWidth();
	createParams.height = skyboxFront.getHeight();
	
	spl::Texture skybox;
	skybox.createNew(createParams);
	
	spl::TextureUpdateParams updateParams;
	updateParams.dataFormat = spl::TextureFormat::RGB;
	updateParams.dataType = spl::TextureDataType::UnsignedByte;
	updateParams.width = skyboxFront.getWidth();
	updateParams.height = skyboxFront.getHeight();
	updateParams.depth = 1;
	
	updateParams.zOffset = 0;
	updateParams.data = skyboxRight.getData();
	skybox.update(updateParams);
	
	updateParams.zOffset = 1;
	updateParams.data = skyboxLeft.getData();
	skybox.update(updateParams);
	
	updateParams.zOffset = 2;
	updateParams.data = skyboxTop.getData();
	skybox.update(updateParams);
	
	updateParams.zOffset = 3;
	updateParams.data = skyboxBottom.getData();
	skybox.update(updateParams);
	
	updateParams.zOffset = 4;
	updateParams.data = skyboxFront.getData();
	skybox.update(updateParams);
	
	updateParams.zOffset = 5;
	updateParams.data = skyboxBack.getData();
	skybox.update(updateParams);

	skyboxRight.createFromFile	("assets/images/skybox2_right.png");
	skyboxLeft.createFromFile	("assets/images/skybox2_left.png");
	skyboxTop.createFromFile	("assets/images/skybox2_top.png");
	skyboxBottom.createFromFile	("assets/images/skybox2_bottom.png");
	skyboxFront.createFromFile	("assets/images/skybox2_front.png");
	skyboxBack.createFromFile	("assets/images/skybox2_back.png");

	createParams.width = skyboxFront.getWidth();
	createParams.height = skyboxFront.getHeight();

	spl::Texture skybox2;
	skybox2.createNew(createParams);

	updateParams.width = skyboxFront.getWidth();
	updateParams.height = skyboxFront.getHeight();

	updateParams.zOffset = 0;
	updateParams.data = skyboxRight.getData();
	skybox2.update(updateParams);

	updateParams.zOffset = 1;
	updateParams.data = skyboxLeft.getData();
	skybox2.update(updateParams);

	updateParams.zOffset = 2;
	updateParams.data = skyboxTop.getData();
	skybox2.update(updateParams);

	updateParams.zOffset = 3;
	updateParams.data = skyboxBottom.getData();
	skybox2.update(updateParams);

	updateParams.zOffset = 4;
	updateParams.data = skyboxFront.getData();
	skybox2.update(updateParams);

	updateParams.zOffset = 5;
	updateParams.data = skyboxBack.getData();
	skybox2.update(updateParams);

	// Main loop

	double t = 0.0;
	while (!window.shouldClose())
	{
		t += 1.0 / 144.0;

		// Handle events

		spl::Event* rawEvent = nullptr;
		while (window.pollEvent(rawEvent))
		{
			switch (rawEvent->type)
			{
			case spl::EventType::ResizeEvent:
			{
				spl::ResizeEvent event = rawEvent->specialize<spl::EventType::ResizeEvent>();
				context->setViewport(0, 0, event.size.x, event.size.y);
				camera.setAspect(event.size.x, event.size.y);
				break;
			}
			}
		}

		// Handle debug messages

		spl::DebugMessage* debugMessage = nullptr;
		while (context->pollDebugMessage(debugMessage))
		{
			std::cout << debugMessage->descr << std::endl;
		}

		// Handle camera

		constexpr float speed = 0.02f;
		if (window.isKeyPressed(spl::KeyboardKey::W)) camera.move(camera.getUpVector() * speed);
		if (window.isKeyPressed(spl::KeyboardKey::S)) camera.move(camera.getUpVector() * -speed);
		if (window.isKeyPressed(spl::KeyboardKey::A)) camera.move(camera.getRightVector() * -speed);
		if (window.isKeyPressed(spl::KeyboardKey::D)) camera.move(camera.getRightVector() * speed);
		if (window.isKeyPressed(spl::KeyboardKey::Space)) camera.move(camera.getFrontVector() * speed);
		if (window.isKeyPressed(spl::KeyboardKey::LeftShift)) camera.move(camera.getFrontVector() * -speed);

		camera.move(camera.getRightVector() * speed / 5.f);

		camera.lookAt({ 0.f, 0.f, 0.f });

		// Draw

		shader.setUniform("cameraPos", camera.getTranslation());
		shader.setUniform("aspect", static_cast<float>(window.getSize().x) / window.getSize().y);

		if (t > 15)
		{
			shader.setUniform("skybox", 0, &skybox2);
		}
		else
		{
			shader.setUniform("skybox", 0, &skybox);
		}

		mesh.draw();

		// Update display

		window.display();
		spl::Framebuffer::clear();
	}
}