#pragma once

#include <chrono>
#include <random>

#include <SciPP/SciPP.hpp>
#include <Diskon/Diskon.hpp>
#include <DejaVu/DejaVu.hpp>
#include <Crozet/Crozet.hpp>
#include <SplayLibrary/SplayLibrary.hpp>
#include <Lyse/Lyse.hpp>

struct SandBoxInfos
{
	const spl::Window* window;
	lys::Scene* scene;
	lys::CameraPerspective* camera;
	double dt;

	void* user;
};


using SandBoxInitFunc = void(*)(SandBoxInfos&);
using SandBoxUpdateFunc = void(*)(SandBoxInfos&);
using SandBoxEndFunc = void(*)(SandBoxInfos&);


class SandBox
{
	public:

		SandBox() = delete;
		SandBox(const std::string& title, SandBoxInitFunc initFunc, SandBoxUpdateFunc updateFunc, SandBoxEndFunc endFunc);
		SandBox(const SandBox& sandbox) = delete;
		SandBox(SandBox&& sandbox) = delete;

		SandBox& operator=(const SandBox& sandbox) = delete;
		SandBox& operator=(SandBox&& sandbox) = delete;

		void run();

		~SandBox() = default;

	private:

		static constexpr scp::u32vec2 _res = { 1000, 600 };
		static constexpr bool _debug = true;
		static constexpr scp::f32vec4 _clearColor = { 0.2f, 0.3f, 0.3f, 1.f };

		SandBoxInfos _infos;

		std::string _title;
		SandBoxInitFunc _initFunc;
		SandBoxUpdateFunc _updateFunc;
		SandBoxEndFunc _endFunc;
};


float random(float min = 0.f, float max = 1.f);

void createVertexGrid(uint64_t n, std::vector<lys::VertexDefaultMesh>& vertices, std::vector<uint32_t>& indices);
void updateVertexGrid(uint64_t n, std::vector<lys::VertexDefaultMesh>& vertices, const scp::f32vec3* pos);

void marchingCube(const bool* data, const scp::i32vec3& size, std::vector<lys::VertexDefaultMesh>& vertices);
