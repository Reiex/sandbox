#pragma once

#include <SandBox/SandBox.hpp>


void gertsnerWavesInit(SandBoxInfos& sandBoxInfos);
void gerstnerWavesUpdate(SandBoxInfos& sandBoxInfos);
void gerstnerWavesEnd(SandBoxInfos& sandBoxInfos);

void phillipsWavesInit(SandBoxInfos& sandBoxInfos);
void phillipsWavesUpdate(SandBoxInfos& sandBoxInfos);
void phillipsWavesEnd(SandBoxInfos& sandBoxInfos);

void lorenzAttractorInit(SandBoxInfos& sandBoxInfos);
void lorenzAttractorUpdate(SandBoxInfos& sandBoxInfos);
void lorenzAttractorEnd(SandBoxInfos& sandBoxInfos);

void rosslerAttractorInit(SandBoxInfos& sandBoxInfos);
void rosslerAttractorUpdate(SandBoxInfos& sandBoxInfos);
void rosslerAttractorEnd(SandBoxInfos& sandBoxInfos);

void lorenzPathInit(SandBoxInfos& sandBoxInfos);
void lorenzPathUpdate(SandBoxInfos& sandBoxInfos);
void lorenzPathEnd(SandBoxInfos& sandBoxInfos);

void lorenzPointsInit(SandBoxInfos& sandBoxInfos);
void lorenzPointsUpdate(SandBoxInfos& sandBoxInfos);
void lorenzPointsEnd(SandBoxInfos& sandBoxInfos);

void nBodyInit(SandBoxInfos& sandBoxInfos);
void nBodyUpdate(SandBoxInfos& sandBoxInfos);
void nBodyEnd(SandBoxInfos& sandBoxInfos);

void boidsInit(SandBoxInfos& sandBoxInfos);
void boidsUpdate(SandBoxInfos& sandBoxInfos);
void boidsEnd(SandBoxInfos& sandBoxInfos);

void erosionInit(SandBoxInfos& sandBoxInfos);
void erosionUpdate(SandBoxInfos& sandBoxInfos);
void erosionEnd(SandBoxInfos& sandBoxInfos);

void mainSphereRaytracing();
