#version 330 core

in vec3 lookingDir;

uniform vec3 cameraPos;
uniform samplerCube skybox;

out vec4 FragColor;

const float pi = 3.1415926;
const float radius = 1.0;
const float eta = 1.6;

vec2 directionToCylindric(in vec3 dir)
{
    vec2 result = vec2(0.0, 0.0);

    vec2 hDir = normalize(dir.xz);
    result.x = atan(hDir.y, hDir.x) / (2.0 * pi);
    result.y = -asin(dir.y) / pi + 0.5;

    return result;
}

vec3 backgroundColor(in vec3 dir)
{
    // return texture(skybox, directionToCylindric(dir)).rgb;
    return texture(skybox, dir).rgb;
}

bool firstIntersection(inout vec3 x, in vec3 dir)
{
    float alpha = dot(x, x);
    float beta = dot(dir, x);
    float delta = beta*beta - alpha + 1.0;

    if (delta < 0)
    {
        return false;
    }
    else
    {
        delta = sqrt(delta);
        float t = -beta - delta;

        if (t < 0)
        {
            t += 2.0*delta; 
        }

        x += dir * t;

        return (t >= 0.0);
    }
}

vec3 nextIntersection(in vec3 x, in vec3 dir)
{
    float alpha = dot(x, x);
    float beta = dot(dir, x);
    float delta = beta*beta - alpha + 1.0;

    return x + dir * (-beta + sqrt(delta));
}

float reflectionIntensity(in vec3 reflectDir, in vec3 normalDir)
{
    const float R0 = ((1.0 - eta) * (1.0 - eta)) / ((1.0 + eta) * (1.0 + eta));
    return clamp(R0 + (1.0 - R0) * pow(1.0 - dot(reflectDir, normalDir), 5), 0.0, 1.0);
}

void main()
{
    vec3 x = cameraPos;
    vec3 dir = normalize(lookingDir);

    if (firstIntersection(x, dir))
    {
        vec3 normalDir = x;
        vec3 reflectDir = reflect(dir, x);
        vec3 refractDir = refract(dir, x, 1.0 / eta);
        
        vec3 color = reflectionIntensity(reflectDir, normalDir) * backgroundColor(reflectDir);

        if (refractDir != vec3(0.0))
        {
            float factor = 1.0 - reflectionIntensity(refractDir, -normalDir);

            dir = refractDir;
            x = nextIntersection(x, dir);

            for (int iter = 0; iter < 10; iter++)
            {
                normalDir = -x;
                reflectDir = reflect(dir, normalDir);
                refractDir = refract(dir, normalDir, eta);

                if (refractDir != vec3(0.0))
                {
                    color += factor * (1.0 - reflectionIntensity(refractDir, -normalDir)) * backgroundColor(refractDir);
                }

                factor *= reflectionIntensity(reflectDir, normalDir);
                
                dir = reflectDir;
                x = nextIntersection(x, dir);
            }
        }

        FragColor = vec4(color, 1.0);
    }
    else
    {
        FragColor = vec4(backgroundColor(dir), 1.0);
    }
}
