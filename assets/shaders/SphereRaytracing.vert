#version 330 core

layout (location = 0) in vec3 aPos;

uniform vec3 cameraPos;
uniform float aspect;

out vec3 lookingDir;

void main()
{
    lookingDir = normalize(-cameraPos);
    vec3 right = normalize(cross(lookingDir, vec3(0.0, 1.0, 0.0)));
    vec3 up = cross(right, lookingDir);
    lookingDir += (right * aPos.x * aspect + up * aPos.y);

    gl_Position = vec4(aPos, 1.0);
}
