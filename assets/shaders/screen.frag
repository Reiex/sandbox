#version 460 core

in vec2 io_texCoords;

uniform sampler2D u_scene;

out vec4 io_fragColor;

void main()
{
    io_fragColor = texture(u_scene, io_texCoords);
} 
