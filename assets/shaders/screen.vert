#version 460 core

layout (location = 0) in vec3 va_position;
layout (location = 3) in vec2 va_texCoords;

out vec2 io_texCoords;

void main()
{
    io_texCoords = va_texCoords;
    gl_Position = vec4(va_position, 1.0);
}
